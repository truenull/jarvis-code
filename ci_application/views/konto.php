<div class='row page-header'>
	<h1>Konto<br/><small><?= $user->user_name ?></small></h1>
</div>

<form id='account-form' class="form-horizontal" role="form">
	<div class="form-group">
		<label for="inputEmail" class="col-md-2 control-label">Email</label>
		<div class="col-md-6">
			<input id='email' name='email' type="email" class="form-control" id="inputEmail" value='<?= $user->email ?>'/>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label">Byta lösenord</label>
		<div class='col-md-3'>
			<input id='newpass' name='newpass' type="password" class='form-control' placeholder="Skriv för att byta"/>
		</div>
		<div class='col-md-3'>
			<input id='newpassrepeat' name='newpassrepeat' type="password" class='form-control' placeholder="Repetera"/>
		</div>
	</div>

	<div class='form-group'>
		<label class="col-md-2 control-label">Nuvarande lösenord</label>
		<div class='col-md-6'>
			<input id='oldpass' name='oldpass' type="password" class='form-control' placeholder="Nuvarande lösenord"/>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-10 col-md-offset-2">
			<button type="submit" class="btn btn-primary">Spara ändringar</button>
		</div>
	</div>

	<div class='form-group'>
		<div id='alerts' class='col-md-6 col-md-offset-2'>
			
		</div>
		<div id='alert-prototype' class='alert' style='display: none'>
			<a class='close' data-dismiss='alert' href='#' aria-hidden='true'>&times;</a>
		</div>
	</div>
</form>

<script type='text/javascript'>
$('#account-form').submit(
	function(e) {
		$('#account-form .btn-primary').text('Sparar ändringar ...');
		$('.form-group').removeClass('has-error');
		$('#alerts').empty();
	
		$.post('<?= site_url('/konto/update_user') ?>', $(this).serializeArray(),
			function(errors) {
				$('#account-form .btn-primary').text('Spara ändringar');
		
				if (errors.length == 0) {
					$('#alert-prototype').clone().removeAttr('id').addClass('alert-success').text('Ändringar sparade.').prependTo('#alerts').show();
				}
				else {
					var alert = $('#alert-prototype').clone().removeAttr('id').addClass('alert-danger');
		
					for (var key in errors) {
						alert.append($('<p>').text(errors[key]));
		
						if (['email', 'newpass', 'oldpass'].indexOf(key) > -1) {
							$('#'+key).closest('.form-group').addClass('has-error');
						}
					}
		
					alert.prependTo('#alerts').show();
				}
			}
		, 'json');

		e.preventDefault;
		return false;
	}
);
</script>