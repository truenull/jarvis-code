<h1 class="row page-header">Databas setup!</h1>
<div class="row">
    <p>Vi försökte uppdatera databasen till <?= $was_error ?>.
</div>
<?php if($error != ''): ?>
<div class="has-error">
    <p> Följande fel skedde:
    <p class="help-block"><?= $error ?>
</div>
<?php else: ?>
    <div class="has-success">
    <h2 class="help-block">Databasinstallation lyckades!</h2>
</div>
<?php endif; ?>

<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <a class="btn btn-primary" href="<?=
    site_url('migrera/');
?>"><span class="glyphicon glyphicon-refresh"> Försök igen</span></a>

        <a class="btn btn-primary" href="<?=
    site_url('login');
?>"><span class="glyphicon glyphicon-arrow-right"> Ok</span></a>
</div>