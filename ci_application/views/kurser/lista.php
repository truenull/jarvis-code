<?php
// Fill in $isteacher if it isn't specified and we have all the variables
// The MVC pattern allows views to access models
// Please don't make a habit of it though...
$ci = &get_instance();

?><div class="page-header row">
    <h1>Kurslista<br/><small><?php echo $user->user_name; ?></small></h1>
</div>
<?= $message ?>
<?php if($isTeacher === TRUE):?>
<div class="row">
    <p><a class="btn btn-primary" href="<?php echo site_url('ny_kurs'); ?>"><span class="glyphicon glyphicon-plus-sign"> Ny kurs</span></a>
<?php endif;
/* @var $courses array */
foreach ($courses as $course):
if($course->course_id != 0)
    $isteacher = $ci->permission_model->is_teacher( $user->user_id, $course->course_id);
else
    $isteacher = false;
        ?>
    <p>
        <?php if($isteacher): ?>
        <a class="btn btn-success"
               href="<?php echo site_url('kurs/andra/' . $course->course_id); ?>" >
                            <span class="glyphicon glyphicon-edit"></span></a>
        <a class="btn btn-danger"
               href="<?php echo site_url('kurs/ta_bort/' . $course->course_id); ?>" >
                            <span class="glyphicon glyphicon-remove"></span></a>
                <?php endif; ?>
        <a class="btn btn-disabled btn-default" href="<?=  site_url('uppgifter/auto/'.$course->course_id); ?>"><?=
                /* @var $course course_record */
                $course->course_name;
                ?>
        </a>
    <?php
endforeach;
?>
</div>