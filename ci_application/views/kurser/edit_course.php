<div class="page-header row  col-lg-12">
    <h1>Ändra  kurs<br/><small><?php
            echo $course->course_name;
        ?> (<?= $course->course_tag ?>)</small>
    </h1>
</div>
<div class="row col-lg-12">
<?php
/* enabling auto-completion of object */
/* @var $course course_record */
echo form_open('kurs/mottag', '',
               array(
        'course_id'     => $course->course_id
            )
    );
    ?>

    <div class="row col-lg-6 col-sm-8 col-md-8">
        <div class="row form-group has-error">
            <?php echo validation_errors(); ?>
        </div>
        
            <div class="row form-group">
                <p class="col-sm-4 col-lg-4 col-md-4 form-inline">Tag:
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <input name="tag" maxlength="10" type="text" class="form-control form-inline disabled col-lg-1 col-md-1 col-sm-1" value="<?= $tag ?>"/>
                </div>
                <p class="col-sm-11 col-lg-11 col-md-11">Du kan skriva in vad-som-helst som Tag. Det behöver inte nödvändigtvis vara kurskoden från PP/LADOK.</p>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Namn:
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
                    <input name="name" maxlength="40" type="text" class="form-control form-inline " value="<?= $name ?>"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Beskrivning (valfritt):
                </div>
                <div class="form-inline col-sm-8 col-lg-8 col-md-8">
                    <textarea name="desc" class="form-control  form-inline"><?= $desc ?></textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Slutdatum:<br/>
                    (används för sortering)
                </div>
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    <input name="end_time" type="date" class="form-control form-inline" <?=
                           ($end_time == '') ?
                                   'placeholder="ex. 2013-12-28' : 'value = "' . $end_time . '"';
                           ?>/>
                </div>
            </div>

           
        <div class="row">
            <p>
                <input type="submit" class="btn btn-primary col-lg-offset-9 col-lg-2 col-md-offset-9 col-md-2 col-sm-offset-9 col-sm-2" value="Spara"/>
        </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>

    <div class='row'>
        <h2>Uppgifter</h2>
        <a href="<?= site_url('uppgift/ny/'.$course->course_id);?>"><button class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span> Lägg till uppgift</button></a>
    </div>
    <div class='row'>
        <table class='table table-striped'>
            <tbody>
            <?php
            foreach ($assignments as $assignment):
                ?>
                <tr>
                    <td><?= $assignment->assignment_number ?></td>
                    <td><?= $assignment->assignment_name ?></td>
                    <td><a href="<?= site_url("uppgift/andra/$course->course_id/$assignment->assignment_id") ?>"><button type='button' class='btn btn-success'><span class='glyphicon glyphicon-edit'></span></button></a></td>
                    <td><a href="<?= site_url("uppgift/ta_bort/$assignment->assignment_id") ?>"><button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove'></span></button></a></td>
                </tr>
                <?php
            endforeach; ?>
            </tbody>
        </table>
    </div>
</div>