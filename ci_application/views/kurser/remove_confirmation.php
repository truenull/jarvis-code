<div class="page-header row">
    <h1>Ta bort kurs<br/>
        <small><?= $course->course_name; ?> (<?= $course->course_tag; ?>)</small></h1>
</div>
    <?=
    form_open('kurs/saker/'.$course->course_id);
?>
<p class="row"> Är du säker på att du vill ta bort denna kurs?</p>
<p class="row"><strong>Alla uppgifter, betyg och filer associerade med kursen kommer också att tas bort.</strong></p>
    <div class="row">
        <div class="row form-group col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">
            <div class="btn-group">
            <a class="btn btn-success form-inline col-lg-5" href="<?= site_url('kurser/lista') ?>">
                <span class="glyphicon glyphicon-backward"> Avbryt</span>
            </a>
            <button type="submit" class="btn btn-danger form-inline col-lg-7">
                <span class="glyphicon glyphicon-remove-sign"> Jag är säker</span>
            </button>
        </div>
        </div>
    </div>
</form>