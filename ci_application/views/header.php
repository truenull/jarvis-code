<!DOCTYPE html><?php
/* @var $course course_record */
//die(print_r($course));
$course_id  = isset($course) ? $course->course_id : '';
 $course_name = isset($course) ? $course->course_name : 'Ingen aktiv kurs';
 $active       = isset($active) ? $active : 0;
 $active_class = ' class="active"';
 
// Fill in $isteacher if it isn't specified and we have all the variables
// The MVC pattern allows views to access models
// Please don't make a habit of it though...
if(!isset($isteacher) && $course_id != '')
{
$ci = &get_instance();

$isteacher = $ci->permission_model->is_admin($ci->user->user_id);
}

 $nav_target = $active != 2 ? 'uppgifter/auto/' : 'deltagare/auto/';
 if (isset($override_nav_target))
     $nav_target = $override_nav_target;
?>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo ((isset($header) ? ($header . " - ") : "") . " webblärsystemet Jarvis"); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A webbased assignment handinsystem">
    <meta name="author" content="Projektgruppen Järv">

    <link rel="stylesheet" href="<?php echo base_url('static/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/bootstrap-theme.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('static/css/joyride-2.1.css'); ?>">


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url('static/js/vendor/jquery-1.10.1.min.js'); ?>"><\/script>');</script>
    <?php
    if (isset($extra_headers))
    {
        foreach ($extra_headers as $header)
        {
        echo $header . "\n";
        }
    }
    ?>

    <link rel="stylesheet" href="<?php echo base_url('static/css/main.css'); ?>">
    <script src="<?php echo base_url('static/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js'); ?>"></script>
    </head>
    <body<?= ((isset($fill) && $fill) ? ' class="nowrap"' : '') ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle navbar-right" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Jarvis</a>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <?php
                        if (!(isset($hideNav) && $hideNav == TRUE)):
                        // don't show nav if the hideNav var is true
?>
                        <li<?= $active == 1 ? $active_class : '' ?>><a href="<?= site_url('uppgifter/auto/' . $course_id) ?>">Uppgifter</a></li>
                        <li<?= $active == 2 ? $active_class : '' ?>><a href="<?= site_url('deltagare/auto/' . $course_id) ?>">Deltagare</a></li>
                        <li<?= $active == 3 ? $active_class : '' ?>><a href="<?= site_url("konto/auto/$course_id") ?>">Mitt Konto</a></li>
                        <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kurs - <?= $course_name ?><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                    <?php
                                                    if (isset($navCourses)):

                                                        foreach ($navCourses as $navCourse):
                                                            /* @var $navCourse course_record */
                                                        ?>
                                                <li><a href="<?=
                                                            site_url($nav_target . $navCourse->course_id);
    ?>"><?= $navCourse->course_name ?></a></li>
                                                        <?php endforeach;
                                                    endif; ?>
                                                    <li class="divider"></li>
                                    <li><a href="<?= site_url('kurser/lista');
                                                ?>">Kurslista</a></li><?php
                                                    if(isset($isteacher) && $isteacher): ?>
                                                    <li class="divider"></li>
                                <li class="dropdown-header">Admin</li>
                                        <li>
                                            <a href="<?=
                                                    site_url('ny_kurs/auto/' . $course_id);
                                                ?>">Skapa ny kurs</a>
                                        </li><?php
                                                endif; ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('login/out'); ?>">Logga ut</a></li>
                        <?php endif; ?>
                    </ul>
                    <?php if(isset($joyride_stops)): ?>
                    <button type="button" onclick="startJoyride();" class="btn btn-info navbar-btn navbar-right"><span class="glyphicon glyphicon-question-sign"></span></button>
                    <?php endif; ?>
                </div><!--/.navbar-collapse -->


            </div>
        </div>
        <ol id="chooseID" class="hide">
            <?php if(isset($joyride_stops)):
             foreach($joyride_stops as $key => $string): ?>
            <li data-id="<?= $key; ?>"><p><?= $string; ?></li>
            <?php endforeach; endif; ?>
        </ol>
        <div class="container col-lg-12 col-md-12 <?= (isset($fill) && $fill) ? ' assignment-grid' : '' ?>">
       