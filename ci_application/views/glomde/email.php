<!-- ./views/glomde/email.php -->
<div class="page-header row  text-center">
    <h1>Lösenordsåterställning</h1>
</div>

<?php
echo form_open_multipart('glomde/formular');
?>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p><input type="text" class="form-control" name="jarvisemail" value="<?php echo set_value('email'); ?>" placeholder="Email"/>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6"
         <div class="btn-group-justified">
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-send"></span> Skicka
            </button>
            </form>
        </div>
    </div>
