<div class="page-header row">
    <h1>Formulär mottaget</h1>
</div>
<div class="row col-lg-6 col-md-6">
    <p>Om det är rätt email så har vi skickat ett mail till dig med
        instruktioner på vad du ska göra nu. Om vi inte har någon användare med
        den adressen har inget mail skickats.
    <p>Om du inte hittar något mail efter att ha kollat i inkorgen och spam kolla om det är rätt email.
        Det är troligtvis den första mailen som listas i PingPong som är den som
        är inlagd i systemet som din email adress.
</div>