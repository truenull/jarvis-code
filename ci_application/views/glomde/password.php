<!-- ./views/glomde/password.php -->
<div class="page-header row text-center">
    <h1>Ändra Lösenord</h1>
</div>
<?php
$validation_errors = validation_errors();
if ($validation_errors != ''):
    ?>

    <div class="row">
        <div class=" has-error col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
            <?php echo $validation_errors; ?>
        </div>
    </div>
<?php
endif;
echo form_open_multipart('glomde/andra');
?>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p>
        <input type="text" class="form-control" name="reset_token" value="<?= (isset($reset_token)) ? $reset_token : set_value('reset_token'); ?>" placeholder="Återställningskod"/>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p><input type="password" class="form-control" name="jarvispass" placeholder="Lösenord"/>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p><input type="password" class="form-control " name="jarvispassconf" placeholder="Bekräfta Lösenord">
</div>
<div class="row">
    <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6"
         <div class="btn-group-justified">
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-send"></span> Skicka
            </button>
            </form>
        </div>
    </div>
