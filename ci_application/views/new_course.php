<div class="page-header row  text-center">
    <h1>Skapa ny kurs</h1>
</div>

<div class="container">
    <?php
    $validation_errors = validation_errors();
    if ($validation_errors != ''):
    ?>
        <div class="row form-group has-error">
            <?php echo $validation_errors; ?>
        </div>
    <?php
    endif;

    $attributes = array('class' => 'form-horizontal', 'id'    => 'new_course');
    echo form_open('ny_kurs/validate/' . $course->course_id, $attributes);
    $first_visit = isset($first_visit) && $first_visit == true ? true : false;
    ?>
    <div class="form-group <?php echo $first_visit == false ? ValidationState('coursetag') : "" ?>">
        <label class="control-label" for="coursetag">Kurstag</label>
        <input type="text" class="form-control" name="coursetag" id="coursetag" placeholder="Kurstag" value="<?php echo set_value('coursetag'); ?>"/>
    </div>
    <div class="form-group <?php echo $first_visit == false ? ValidationState('coursename') : "" ?>">
        <label class="control-label" for="coursename">Kursnamn</label>
        <input type="text" class="form-control" name="coursename" id="coursename" placeholder="Kursnamn"value="<?php echo set_value('coursename'); ?>"/>
    </div>
    <div class="form-group <?php echo $first_visit == false ? ValidationState('courseend') : "" ?>">
        <label class="control-label" for="courseend">Slutdatum</label>
        <input type="text" class="form-control" name="courseend" id="courseend" placeholder="YYYY-MM-DD" value="<?php echo set_value('courseend'); ?>"/>
    </div>
    <div class="form-group <?php echo $first_visit == false ? ValidationState('coursedescription') : "" ?>">
        <label class="control-label" for="coursedescription">Kursbeskrivning</label>
        <textarea class="form-control" rows="3" name="coursedescription" id="coursedescription" placeholder="Kursbeskrivning" form="new_course"><?php echo set_value('coursedescription'); ?></textarea>
    </div>
    <button type="submit" class="btn btn-primary">
        <span class="glyphicon glyphicon-log-in"></span> Skapa kurs
    </button>
    </form>
</div>
