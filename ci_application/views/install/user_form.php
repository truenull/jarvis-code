<h1 class="row page-header">Administratörsanvändare</h1>
<?php $errors = validation_errors();
if($errors != ''): ?>
<div class="row form-group has-error">
    <div class="help-block">
    <?= $errors?>
    </div>
</div>
<?php endif; ?>
<?= form_open('install/administrator'); ?>
<div class="row form-group">
    <div class="container col-lg-10 col-md-10 col-sm-10">
        <div class="row form-group">
            <span class="col-lg-3 col-md-3 col-sm-3 form-horizontal">Namn:</span>
            <span class="col-lg-6 col-md-6 col-sm-6">
            <input name="name" autofocus placeholder="ex. Ragnar Nohre"
                   class="form-control form-inline col-lg-4 col-md-4 col-sm-4"
                   type="text" value="<?= set_value('name')?>" required/>
            </span>
        </div>
        <div class="form-group row">
            <span class="col-lg-3 col-md-3 col-sm-3 form-inline">Email:</span>
            <span class="col-lg-6 col-md-6 col-sm-6">
                <input name="email" placeholder="ex. ragnar@nanting.se"
                       class="form-control form-inline" value="<?= set_value('email')?>" type="email" required/>
            </span>
        </div>
        <div class="form-group row">
            <span class="col-lg-3 col-md-3 col-sm-3 form-inline">Nytt lösenord:</span>
            <span class="col-lg-6 col-md-6 col-sm-6">
                <input name="jarvispass" class="form-control form-inline "
                       maxlength="<?= user_model::max_pass_length?>" minlength="<?= user_model::min_pass_length?>"
                       placeholder="<?= user_model::min_pass_length?> till <?= user_model::max_pass_length?> tecken" type="password" required/>
            </span>
        </div>
    </div>
</div>
<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <button class="btn btn-primary" type="submit">Skapa</button>
</div>
</form>