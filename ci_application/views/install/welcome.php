<h1 class="row page-header">Välkommen!</h1>
<div class="row">
    <p>Om du ser denna sida utan en svart navbar högst upp så är det troligt att du inte konfigurerat installationen ordentligt.
    <p>Försök att kolla:
    <ul>
        <li>Att du laddat upp static mappen i samma mapp som index.php</li>
        <li>Att base_url variabeln i ci_application/config/config.php är satt till samma mapp som index.php filen.</li>
        <li>.htaccess url omskrivning ändrats på rätt sätt (se README filen)<br/>
        Det är dock troligt att det är en av de första punkterna är orsaken till felet.</li>
    </ul>
</div>
<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <a class="btn btn-primary" href="<?=
    site_url('install/databas');
?>"><span class="glyphicon glyphicon-arrow-right">Nästa</span></a>
</div>