<h1 class="row page-header">Hejsan utvecklare!</h1>
<div class="row">
    <p>Du ska nu kunna logga in med lösenordet "i am im" på följande konton.
    <ul>
        <li>admin@nowhere.invalid</li>
        <li>teacher@nowhere.invalid</li>
        <li>limitedteacher@nowhere.invalid</li>
        <li>student@nowhere.invalid</li>
    </ul>
</div>
<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <a class="btn btn-primary" href="<?=
    site_url('install/filer');
?>"><span class="glyphicon glyphicon-arrow-right"> Nästa</span></a>
</div>