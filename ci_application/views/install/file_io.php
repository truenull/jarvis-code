<h1 class="row page-header">Filrättigheter</h1>
<div class="row">
    <p>Vi använder '<?= htmlspecialchars($upload_path); ?>' när vi sparar filer.
        <p>Den <?= $writable? 'är':'behöver vara'; ?> skrivbar.
        <p>Om du vill kan du ändra i <i>ci_application/config/upload.php</i> 
            för att välja en annan plats
</div>
<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <a class="btn btn-primary" href="<?=
    site_url('login');
?>"><span class="glyphicon glyphicon-arrow-right"> Klar</span></a>
</div>