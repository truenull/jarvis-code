<h1 class="row page-header">Administratörsanvändare</h1>
<div class="row">
    <p>Din användare har skapats. Du bör ha fått ett mejl från servern om
        mailkonfigurationen är korrekt.
</div>
<div class="row col-lg-offset-7 col-md-offset-7 col-sm-offset-7 col-xs-offset-3">
    <a href="<?= site_url('install/filer'); ?>" class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-arrow-right"> Filhantering</span></a>
</div>
</form>