<h1 class="row page-header">Databas setup!</h1>
<div class="row">
    <p>Eftersom vi autoladdar databasen så är din konfiguration korrekt om du kan se detta.
</div>
<?php if($error != ''): ?>
<div class="has-error">
    <p class="help-block"><?= $error ?>
        <p> Du bör försöka lösa detta fel innan du går vidare.
</div>
<?php else: ?>
    <div class="has-success">
    <h2 class="help-block">Databasinstallation lyckades!</h2>
</div>
<?php endif; ?>

<div class="row col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-xs-offset-2">
    <?php if($error != ''): ?>
    <a class="btn btn-primary" href="<?=
    site_url('install/databas');
?>"><span class="glyphicon glyphicon-refresh"> Försök igen</span></a>
<?php endif; ?>
        <a class="btn btn-primary" href="<?=
    site_url('install/admin');
?>"><span class="glyphicon glyphicon-arrow-right"> Skapa administratör</span></a>
<?php if (ENVIRONMENT == 'development'): ?>
        <a class="btn btn-primary" href="<?=
    site_url('install/exempel');
?>"><span class="glyphicon glyphicon-arrow-right"> För in exempeldata</span></a>
<?php endif; ?>
</div>