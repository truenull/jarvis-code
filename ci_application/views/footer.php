<!-- beginning of ./views/footer.php-->
</div> <!--/container-->

<hr>
<footer class="text-center">
    <p>&copy; Projektgruppen Järv 2013</p>
</footer>
<script src="<?php echo base_url('static/js/vendor/bootstrap.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('static/js/plugins.js'); ?>"></script>
<script src="<?php echo base_url('static/js/main.js'); ?>"></script>

<script src="<?php echo base_url('static/js/vendor/jquery.joyride-2.1.js'); ?>"></script>

<script type="text/javascript">
    function startJoyride() {
        $("#chooseID").joyride({
            'auto-start' :true
        });
        $("#chooseID").joyride();
    }
</script>
</body>
</html>