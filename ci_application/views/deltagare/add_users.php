<div class="page-header col-lg-12"><h1>
        Lägg till deltagare<br/><small><?php
echo $course->course_name;
?> ( <?=$course->course_tag; ?> )</small></h1></div>
<div class="row col-lg-12">
<?php
/* enabling auto-completion of object */
/* @var $course course_record */
echo form_open_multipart('deltagare/mottag', '',
               array(
        'course_id'     => $course->course_id,
            )
    );
$errors = validation_errors();
    ?>

    <div class="row col-lg-6 col-sm-8 col-md-8">
        <?php if($errors != ''): ?>
        <div class="row form-group has-error">
            <?= $errors;  ?>
        </div>
        <?php endif; ?>
            <label class="row">Enstaka</label>
            <div class="form-group row">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    <label for="namef">Namn:</label>
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
                    <input id="namef" name="namef" class="form-control form-inline " type="text" value="<?= set_value('namef'); ?>"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                      <label for="emailf">Email:</label>
                </div>
                <div class="form-inline col-sm-8 col-lg-8 col-md-8">
                    <input type="email" name="emailf" id="emailf" class="form-control form-inline" value="<?= set_value('emailf'); ?>"/>
                </div>
            </div>

                <label for="userfile" class="row">CSV fil:</label>
            <div class="form-group row">
                <input type="file" name="userfile" id="userfile">
                <div class="help-block col-lg-12 col-md-12 col-sm-12">
                    <p>CSV filen måste ha formen:
                    <p>Förnamn;EfterNamn;mail1@blah.com, mail2@blah.se;<br/>
                    Förnamn;EfterNamn;mail3@blah.com;
                    <p>Mail fältet kan ha hur många email separerade av kommatecken som helst. Den första är dock den som används.
                <div>
            </div>
        <div class="form-group">
                <input type="submit" class="btn btn-primary col-lg-offset-7 col-lg-2 col-md-offset-7 col-md-2 col-sm-offset-7 col-sm-2" value="Skapa"/>
        </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>