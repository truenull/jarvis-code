<div class="page-header row  col-lg-12"><h1>
        Ändra deltagare<br/><small><?php
echo $user->user_name;
?> ( <?=$user->email; ?> )</small></h1></div>
<div class="row col-lg-12">
<?php
/* enabling auto-completion of object */
/* @var $course course_record */
/* @var $user user_record */
echo form_open('anvandare/mottag', '',
               array(
        'course_id'     => $course->course_id,
        'user_id' => $user->user_id
            )
    );
    ?>

    <div class="row col-lg-6 col-sm-8 col-md-8">
        <div class="row form-group has-error">
            <?php echo validation_errors(); ?>
        </div>
        
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Namn:
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
                    <input name="namef" class="form-control form-inline " type="text" value="<?= $name ?>"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Email:
                </div>
                <div class="form-inline col-sm-8 col-lg-8 col-md-8">
                    <input type="email" name="emailf" class="form-control form-inline" value="<?= $email ?>"/>
                </div>
            </div>
            <?php if($user_is_admin == TRUE): ?>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Systemadministratör:
                </div>
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    <input name="is_admin" value="order66" 
                           class="form-control form-inline" type="checkbox"<?=
                        $is_admin? ' checked':'' ?><?= ($is_self?' disabled':'');?>/>
                </div>
            </div>
            <?php endif; ?>
        <div class="row">
            <p>
                <input type="submit" class="btn btn-primary col-lg-offset-9 col-lg-2 col-md-offset-9 col-md-2 col-sm-offset-9 col-sm-2" value="Spara"/>
        </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>