<!-- ./views/login_form.php -->
<div class="page-header row  text-center">
    <h1>Inloggning</h1>
</div>
<?php
$validation_errors = validation_errors();
if ($validation_errors != ''):
    ?>

        <div class="row">
            <div class=" has-error col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
                    <?php echo $validation_errors; ?>
                </div>
            </div>
<?php endif; 
echo form_open_multipart('login/in');
?>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p><input type="text" class="form-control" name="jarvisemail" value="<?php echo set_value('email'); ?>" placeholder="Email"/>
</div>
<div class="row col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
    <p><input type="password" class="form-control " name="jarvispass" placeholder="Lösenord">
</div>
<div class="row">
    <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-6 col-md-6 col-lg-6"
         <div class="btn-group-justified">
             <button type="submit" class="btn btn-primary">
                     <span class="glyphicon glyphicon-log-in"></span> Logga in
             </button>
             <a  href="<?php echo site_url('glomde/'); ?>" class="btn btn-info">
                 <span class="glyphicon glyphicon-refresh">
            </span> Glömt lösenord
             </a>
         </form>
        </div>
    </div>
