<!-- ./views/uppgifter/teacher.php -->
<!--Modal--><?php
/* @var $course course_record */
/* @var $turnins array(student_turnin_record) */
/* @var $turnin student_turnin_record */
// Note: use $turnin as the foreach var to store $turnins as
/* @var $maxAssNum int */
//die(print_r($turnins));
$maxAssNum    = -1;
/* @var $minAssNum int */
$minAssNum    = -1;
// needed to know when to stop itterating over turins to output an assignment
?>

<div class="row">
    <h1 class="page-header">Uppgiftsmatris <small>(<?= $course->course_name ?>)</small></h1>
        <?php if($course->course_description != ''): ?><p><strong>Kursbeskrivning:</strong> <?=
            htmlentities($course->course_description); endif;?>
    <?= $op_text ?>
</div>
<div class="row">
    <!--Using vertical to make all name buttons equal width.-->
    <div class="btn-group-vertical">
        <!--The top button is invisible to line up the numbers and names with the other buttons, thus making a grid.-->
        <?=
        uppgift_nav(
                $course->course_id, 0, 1); ?>
        <div class="modal fade invisible" tabindex="-1" role="dialog" aria-hidden="true">
        </div><!-- /.modal -->
        <?php
        $nameCurAssId = -100;
        $first_row = true;
// Ouput all names
        foreach ($turnins as $turnin):
            ?><?php
            // stopping and starting phptag is just to get auto-indent to work.
            if ($nameCurAssId < 0)
            {
                $nameCurAssId = $turnin->assignment_id;
            }
            else if ($nameCurAssId != $turnin->assignment_id)
            {
                // Break if we hit the second turnin
                break;
            }
            ?>

        <a <?= ($first_row) ? ' id="userLinkStop"' : ''
            ?>class="btn btn-default btn-sm" href="<?php
               echo site_url('uppgifter/elev/' . $turnin->user_id . '/' . $course->course_id);
?>"><span class="glyphicon glyphicon-fullscreen"> <?= $turnin->user_name ?></span></a>

        <div class="modal fade invisible" tabindex="-1" role="dialog" aria-hidden="true">
                        </div><!-- /.modal --><?php
          $first_row = false;
         endforeach; ?>
    </div>
        <?php
    $curAssId = -100;
        $lastAssId = -100;
        $new_col   = TRUE;
        $first_row = TRUE;
?>
        <?php foreach ($turnins as $turnin): ?><?php
            $curAssId = intval($turnin->assignment_id);
            // Check if assignment changed.
            if ($lastAssId != intval($turnin->assignment_id))
            {
                $new_col = TRUE;
            }
            if ($new_col):
                ?>
                <?php if (!$first_row): ?>

                </div>
 <?php
            endif; // !$firstrow
            ?><div class="btn-group-vertical">
                <a href="<?= site_url('uppgift/andra/'. $turnin->course_id .'/'.$turnin->assignment_id);
                    ?>" class="btn btn-<?=
            mandatory_button($turnin->mandatory); ?> btn-sm"><?=
                    $turnin->assignment_number ?>
                </a><?php
            endif; // $newcol
                    ?>

                <a class="btn<?= status_css($turnin->status);
            ?> btn-sm" href="<?=
    site_url('uppgifter/teacher_nonmodal/'
            . $turnin->user_id . '/'
            . $turnin->assignment_id);
            ?>" data-toggle="modal" data-remote="<?=
    site_url('uppgifter/teacher_modal/'
            . $turnin->user_id . '/'
            . $turnin->assignment_id)
    ?>" data-target="#teacherModal<?= $turnin->user_id . '-' . $turnin->assignment_id;
    ?>"><span<?= ($first_row) ? ' id="modalButtonStop"' : ''
            ?> class="glyphicon<?= status_icon($turnin->status); ?>"> </span></a>
                        <div class="modal fade" id="teacherModal<?= $turnin->user_id ?>-<?= $turnin->assignment_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
                        </div><!-- /.modal -->
                        <?php
                        if ($new_col)
        {
            $lastAssId = $curAssId;
        }
        $new_col   = FALSE;
        $first_row = FALSE;
    endforeach;
                ?>
            </div>

</div> <!--/row-->