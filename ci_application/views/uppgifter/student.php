<!-- ./views/uppgifter/teacher.php --><?php /* @var $user user_record */
$modal_url = $isteacher? 'uppgifter/teacher':'uppgifter/student';
?>
<div class="container">
    <div class="page-header row">
        <h1>Upgifter i <?= $course->course_name; ?> <br/><small><?= isset($user) ? $user->user_name : ''; ?> ( <?= isset($user) ? $user->email : ''; ?> )</small></h1>
        <?php if($course->course_description != ''): ?><p><strong>Kursbeskrivning: </strong><?=
            htmlentities($course->course_description); endif;?>
            <p>Ackumulerade poäng: <?=count($accumulated_points)>0?$accumulated_points[0]->grade:0;?></p>
            <p>Möjliga poäng: <?=$max_points?></p>

            <?php if ($isteacher): ?>
            <div class="btn-group">
                 <a href="<?= site_url('uppgifter/matris/' . $courseid) ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"> Lärarvy</span></a>
                 <?= uppgift_nav($course->course_id); ?>
            </div>
        <?php endif; ?>
        </div>

        <div class="row">
            <div class="">
            <?php
            $this->load->helper('misc_helper');
            $first_row = true;
            foreach($turnins as $turnin): ?>
                        <p><div class="btn-group">
                        <a <?= $first_row?'id="modalButtonStop" ':''; ?>class="btn<?= status_css($turnin->status) ?>" href="<?=
                           site_url($modal_url.'_nonmodal/' . $turnin->user_id . '/'
                                   . $turnin->assignment_id)
    ?>" data-toggle="modal" data-remote="<?=
                           site_url($modal_url . '_modal/'
                                   . $turnin->user_id . '/'
                                   . $turnin->assignment_id)
                           ?>" data-target="#studentModal<?= $turnin->user_id ?>-<?= $turnin->assignment_id ?>">
                        <span class="glyphicon <?= status_icon($turnin->status) ?>"></span>
                            </a>

                            <a class="btn btn-<?= mandatory_button($turnin->mandatory) ?>" href="<?=
                           site_url($modal_url . '_nonmodal/' . $turnin->user_id . '/'
                                   . $turnin->assignment_id)
    ?>" data-toggle="modal" data-remote="<?=
                           site_url($modal_url . '_modal/'
                                   . $turnin->user_id . '/'
                                   . $turnin->assignment_id)
                           ?>" data-target="#studentModal<?= $turnin->user_id . '-' . $turnin->assignment_id; ?>">
                                    <span class="glyphicon glyphicon-chevron-right"> 
                                        <?= trim($turnin->assignment_name); ?>
                                    </span>
                                </a>

                                <?php if ($isteacher): ?>
                                    <?=
                                    uppgift_remove($turnin->assignment_id).
                                    uppgift_nav(
                                            $course->course_id,
                                            $turnin->assignment_id);
                                    ?>
                                <?php endif; ?>
                </div>

            <div class="modal fade" id="studentModal<?= $turnin->user_id ?>-<?= $turnin->assignment_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <p>placeholder för elevmodalen</p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>