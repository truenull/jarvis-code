<?php
// Needed to make the modals that are loaded not have duplicate id for codeviews
$unique_number = $user->user_id . '-' . $assignment->assignment_id;
// needed to autoload url of first file i list into codeview.
$first_url     = '';
?><div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <?php if (!isset($nonmodal) || isset($nonmodal) && !$nonmodal): ?>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <?php endif; ?>
            <h4 class="modal-title">Uppgift <?= $assignment->assignment_number ?> - <?= $assignment->assignment_name; ?></h4>
        </div><?= form_open_multipart('uppgifter/student_submit/'
                            . $user->user_id . "/" . $assignment->assignment_id
                            . "/" . $assignment->course_id,
                                       array(
                                           'onsubmit' => 'return validate()',
                                           'id'       => "student_form$unique_number"
                            )); ?>
        <div class="modal-body" role="navigation">
            <div class="navbar">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Filer<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php
                            // Print all available files.
                            foreach ($assignment_files as $key => $value):
                                
                $load_url = site_url('fil/ladda/' . $user->user_id . '/' . $value->assignment_file_id);
                if ($first_url == '')
                {
                    if($value->download_only == FALSE)
                    {
                        $first_url = $load_url;
                    }
                }?>
                            <?php if($value->download_only == FALSE): ?>
                            <li class="dropdown-header"><?=
                            $value->file_name; ?></li>
                            <li><a onclick="$('#codeview<?= $unique_number;
                    ?>').load('<?= $load_url; ?>');" href="#"><span class="glyphicon glyphicon-file"> Visa</span></a>
                                
                            </li>
                            <?php endif; ?>
                            <li>
                                <a target="_blank" href="<?= site_url('fil/nerladdning/'
                                        . $user->user_id . '/'
                                        . $value->assignment_file_id); ?>">
                                    <span class="glyphicon glyphicon-download-alt">
                                        Ladda ner
                                    </span>
                                </a>
                            </li>
                            <li>
                                <?php if($himself): ?>
                                <a href="<?= site_url('fil/ta_bort/'
                                        . $user->user_id . '/'
                                        . $value->assignment_file_id); ?>">
                                    <span class="glyphicon glyphicon-remove">
                                        Ta bort
                                    </span>
                                </a>
                                <?php endif; ?>
                            </li>
                            <li class="divider"></li><?php
                                endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="codeview<?= $unique_number; ?>">
            </div>
            <div class="container">
                <?php if($assignment->assignment_desc != ''): ?><p class="row"><strong>Uppgiftsbeskrivning: </strong><?=
            htmlentities($assignment->assignment_desc); endif;?>
                <div class="row">
                    <!--grading part-->
                    <div class="col-md-3">
                        <h4>Filer:</h4>
                        <?php

                        // Print all available files.
                        foreach ($assignment_files as $key => $value)
                        {
                            echo "<p>";
                            echo "$value->file_name"; //Filename to show
                            echo ': <input size="2" name="file'
                            .$value->assignment_file_id . '" type="file"/>';
                        }
                        ?>
                        <!--<input id="hiddensubmit<?=$unique_number?>" type="submit" value="Skicka" style="display:none">-->
                        <h4>Resultat:</h4>
                        <p id="result"></p>
                    </div>

                    <!-- kommentars fält -->
                    <div class="col-md-7 wrap">
                        <h4>Kommentarer:</h4>
                        <?php
                        // Print all comments.
                        foreach ($assignment_comments as $key => $value)
                        {
                            $comment = htmlspecialchars($value->comment);
                            echo "<p class=\"break-word\"><strong>$value->commenter_name:</strong> $comment</p>";
                        }
                        ?>
                        <p><textarea name="comment" placeholder="Skriv meddelande för ändringen."></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php if (!isset($nonmodal) || isset($nonmodal) && !$nonmodal): ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Stäng</button>
            <?php endif; ?>
            <button class="btn btn-primary" type="submit">Skicka</button>
        </div>
                        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script type="text/javascript">
    // Set the status and grade as string.
    $(document).ready(function()
    {
        // Get status string.
        var status = <?= isset($assignment_status->status) ? $assignment_status->status : "" ?>;
        var status_string;
        if (status === 0)
            status_string = "Ej granskad eller ej inlämnad";
        else if (status === 10)
            status_string = "Kompletteras";
        else if (status === 20)
            status_string = "Underkänd";
        else if (status === 30)
            status_string = "Godkänd";
        else
            status_string = "ERROR: Status " + status + " ej känd";

        // Get grade string.
        var grade = <?= isset($assignment_status->grade) ? $assignment_status->grade : 0 ?>;
        var max_grade = <?= isset($assignment->max_points) ? $assignment->max_points : 0 ?>;
        var grade_string;
        if (max_grade === 0)
            grade_string = ".";
        else
            grade_string = " - " + grade + " av " + max_grade + " poäng.";

        // Set status and grade.
        $('#result').html(status_string+grade_string);
    });

    // Validate the form and if successful return true for onsubmit.
    function validate()
    {
        // TODO: Add validation for the files
        
        // Validation ok
        return true;
    }

    // Load the first file in the codeview if its url is not empty
    <?php if($first_url != ''): ?>
    //$('#codeview<?= $unique_number ?>').load('<?= $first_url ?>');
    <?php endif; ?>
</script>