<div class="page-header row  col-lg-12"><h1><?php
        if ($id === 0)
        {
            echo 'Ny';
        }
        else
        {
            echo 'Ändra';
        }
        ?> uppgift<br/><small><?php
echo $course->course_name;
?></small></h1></div>
<div class="row col-lg-12">
<?php
/* enabling auto-completion of object */
/* @var $course course_record */
echo form_open('uppgift/mottag', '',
               array(
        'course_id'     => $course->course_id,
        'assignment_id' => $id
            )
    );
    ?>

    <div class="row col-lg-6 col-sm-8 col-md-8">
        <div class="row form-group has-error">
            <?php echo validation_errors(); ?>
        </div>
        
            <div class="row form-group">
                <p class="col-sm-4 col-lg-4 col-md-4 form-inline">Nummer:
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <input name="number" type="text" class="form-control form-inline col-lg-1 col-md-1 col-sm-1" value="<?= $number ?>"  id="numRangeDisplay"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Namn:
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
                    <input name="name" class="form-control form-inline " type="text" value="<?= $name ?>"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Beskrivning (valfritt):
                </div>
                <div class="form-inline col-sm-8 col-lg-8 col-md-8">
                    <textarea name="desc" class="form-control  form-inline"><?= $desc ?></textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    Sista inlämning (t.o.m):
                </div>
                <div class="form-inline col-sm-4 col-lg-4 col-md-4">
                    <input name="due_date" class="form-control form-inline" type="date" <?php
                           echo $due_date == '' ? 'placeholder="ex. ' . date('Y-m-d') : 'value = "' . $due_date; ?>"/>
                           </div>
            </div>
            
            <div class="row form-group">
                <p class="form-inline col-sm-4 col-lg-4 col-md-4">Max poäng:
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <input name="max_points" type="text" class="form-control form-inline disabled col-lg-1 col-md-1"
                           value="<?= $max_points ?>"  id="rangeDisplay" />
                </div>
            </div>
            <div class="row form-group">
                <p class="form-inline col-sm-4 col-lg-4 col-md-4">Obligatorisk:
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <input name="mandatory" type="checkbox"class="form-control form-inline col-lg-1 col-md-1" 
                           value="1"<?= $mandatory == 0 ? '' : ' checked' ?> id="rangeDisplay" />
                </div>
            </div>
            <div class="row form-group">
                <div class="form-inline  col-sm-4 col-lg-4 col-md-4">
                    Filer (en per rad):
                </div>

                <div class="form-inline  col-sm-8 col-lg-8 col-md-8">
                    <textarea name="files" class="form-control form-inline"><?= $files ?></textarea>
                </div>
                <p class="help-block col-sm-9 col-lg-9 col-md-9">Filer som enbart ska laddas ner har ett filnamn som börjar på '+'.
            </div>
        
        <div class="row">
            <p>
                <input type="submit" class="btn btn-primary col-lg-offset-9 col-lg-2 col-md-offset-9 col-md-2 col-sm-offset-9 col-sm-2" value="Spara"/>
        </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>