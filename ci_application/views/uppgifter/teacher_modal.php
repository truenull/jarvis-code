<?php
// Needed to make the modals that are loaded not have duplicate id for codeviews
$unique_number = $user->user_id . '-' . $assignment->assignment_id;
// needed to autoload url of first file i list into codeview.
$first_url      = '';
?><div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <?php if (!isset($nonmodal) || isset($nonmodal) && !$nonmodal): ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <?php endif; ?>
            <h4 class="modal-title">Uppgift <?= $assignment->assignment_number ?> - <?= $assignment->assignment_name ?> - <?= $user->user_name ?></h4>
        </div>

        <?php
        $attributes = array('onsubmit' => 'return validate()',
            'id'       => "form$unique_number");
        echo form_open('uppgifter/teacher_submit/' . $user->user_id . "/" . $assignment->assignment_id . "/" . $assignment->course_id,
                       $attributes);
        ?>
        <div class="modal-body" role="navigation">
            <div class="navbar">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Filer<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php
                            // Print all available files.
                            foreach ($assignment_files as $key => $value):
                                $load_url = site_url('fil/ladda/' . $user->user_id . '/' . $value->assignment_file_id);
                            if ($first_url == '')
                            {
                                if($value->download_only == FALSE)
                                {
                                    $first_url = $load_url;
                                }
                            }
                            ?>
                                <?php if($value->download_only == FALSE): ?>
                            <li class="dropdown-header"><?=
                            $value->file_name; ?></li>
                            <li><a onclick="$('#codeview<?= $unique_number;
                    ?>').load('<?= $load_url; ?>');" href="#"><span class="glyphicon glyphicon-file"> Visa</span></a>

                            </li>
                            <?php endif; ?>
                            <li>
                                <a target="_blank" href="<?= site_url('fil/nerladdning/'
                                        . $user->user_id . '/'
                                        . $value->assignment_file_id); ?>">
                                    <span class="glyphicon glyphicon-download-alt">
                                        Ladda ner
                                    </span>
                                </a>
                            </li>
                            <li class="divider"></li><?php
                            endforeach; ?>
                                
                        </ul>
                    </li>
            </ul></div>
            <div id="codeview<?= $unique_number ?>">
            </div>
            <div class="container">
                <div class="row">
                    <?php if($assignment->assignment_desc != ''): ?><p class="row"><strong>Uppgiftsbeskrivning:</strong> <?=
            htmlentities($assignment->assignment_desc); endif;?>
                    <!--grading part-->
                    <div class="col-md-3">
                        <h4>Betyg:</h4>
                        <p>Godkänd: <input name="betyg" type="radio" value="30" <?= isset($assignment_status->status) && $assignment_status->status == 30 ? "checked=\"checked\"" : "" ?>/>
                        <p>Komplettera: <input name="betyg" type="radio" value="0" <?= isset($assignment_status->status) && $assignment_status->status == 0 ? "checked=\"checked\"" : "" ?>/>
                        <p>Underkänd: <input name="betyg" type="radio" value="20" <?= isset($assignment_status->status) && $assignment_status->status == 20 ? "checked=\"checked\"" : "" ?>/>
                        <p>Ej granskad: <input name="betyg" type="radio" value="0" <?= isset($assignment_status->status) && $assignment_status->status == 0 || isset($assignment_status->status) && $assignment_status->status == 5 ? "checked=\"checked\"" : "" ?>/>
                        <p id="p_grade<?=$unique_number?>">Poäng: <input size="2" type="text" name="grade" id="grade<?=$unique_number?>" class="form-control"
                            <?php
                            // TODO: This looks ugly, clean it up.
                            // Set input value.
                            if (!isset($assignment->due_date))
                            {
                                if (isset($assignment_status->grade))
                                    echo "value=\"" . $assignment_status->grade . "\"";
                                else
                                    echo "value=\"\"";
                            }
                            else if ( !isset($assignment_status->grade) || time() <= $assignment->due_date && $assignment_status->grade == 0)
                                echo "value=\"\"";
                            else
                                echo "value=\"" . $assignment_status->grade . "\"";

                            // Set input placeholder.
                            if (!isset($assignment->due_date) || time() <= $assignment->due_date || $assignment->due_date == 0)
                            {
                                if (isset($assignment->max_points))
                                    echo "placeholder=\"" . $assignment->max_points . "\"";
                                else
                                    echo "placeholder=\"0\"";
                            }
                            else
                                echo "placeholder=\"0\"";
                            ?>
                                         /></p>
                    </div>

                    <!-- kommentarsfält -->
                    <div class="col-md-7 wrap">
                        <h4>Kommentarer:</h4>
                        <?php
                        // Print all comments.
                        foreach ($assignment_comments as $key => $value)
                        {
                            $comment = htmlspecialchars($value->comment);
                            echo "<p class=\"break-word\"><strong>$value->commenter_name:</strong> $comment</p>";
                        }
                        ?>
                        <p><textarea id ="comment<?=$unique_number?>" name="comment" placeholder="Skriv meddelande för ändringen."></textarea>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-body -->

        <div class="modal-footer">
            <?php if (!isset($nonmodal) || isset($nonmodal) && !$nonmodal): ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Stäng</button>
            <?php endif; ?>
                <input class="btn btn-primary" type="submit" value="Skicka"/>
        </div>
    </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script type="text/javascript">
    // Load the first file in the codeview if its url is not empty
    <?php if($first_url != ''): ?>
    $('#codeview<?= $unique_number ?>').load('<?= $first_url ?>');
    <?php endif; ?>
        
    // Validate the form and if successful return true for onsubmit.
    function validate()
    {
        // Validate grade.
        var grade = document.getElementById('grade<?=$unique_number?>').value;
        var max_points = <?php echo isset($assignment->max_points) ? $assignment->max_points : 0; ?>;
        if (grade !== "" && (!isInt(grade) || grade < 0 || grade > max_points))
        {
            document.getElementById('p_grade<?=$unique_number?>').className = "has-error";
            return false;
        }
        
        // Validate status is not needed as we accept empty value.

        // Validation ok
        return true;
    }

    function isInt(n)
    {
        return !isNaN(parseFloat(n)) && isFinite(n) && n % 1 === 0;
    }
</script>
