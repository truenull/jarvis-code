<div class="page-header row"><?php
    /* @var $assignment assignment_record */
    /* @var $course course_record */
    ?><h1>Ta bort uppgift<br/>
        <small><?= $assignment->assignment_name; ?> (<?= $course->course_tag; ?>)</small></h1>
</div>
    <?=
    form_open('uppgift/saker/'.$assignment->assignment_id);
?><input name="confirmation" type="hidden" value="yes"/>
<p class="row"> Är du säker på att du vill ta bort denna uppgift?</p>
<p class="row"><strong>Alla betyg och filer associerade med kursen kommer också att tas bort.</strong></p>
    <div class="row">
        <div class="row form-group col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">
            <div class="btn-group">
            <a class="btn btn-success form-inline col-lg-5" href="<?= site_url('uppgifter/auto/'.$course->course_id) ?>">
                <span class="glyphicon glyphicon-backward"> Avbryt</span>
            </a>
            <button type="submit" class="btn btn-danger form-inline col-lg-7">
                <span class="glyphicon glyphicon-remove-sign"> Jag är säker</span>
            </button>
        </div>
        </div>
    </div>
</form>