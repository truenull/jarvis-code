<?php
/**
 * This file contains the base controller for the secured pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Handles session validation.
 *
 * Redirects if validation fails and stores the current user if it succeeds.
 *
 * @property user_model $user_model Only available if it is loaded (or on auto-load)
 * @property permission_model $permission_model Only available if it is loaded (or on auto-load)
 * @property course_model $course_model Only available if it is loaded (or on auto-load)
 * @property assignment_model $assignment_model Only available if it is loaded (or on auto-load)
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class My_Controller extends CI_Controller
{
    /**
     *
     * @var user_record $user
     */
    public $user;

    /**
     * 
     */
    function __construct()
    {

        parent::__construct();

        check_db_version();
        
        $this->load->library('session');

        $user_id = $this->session->userdata('user_id');

        if ($user_id === FALSE)
        {
            // redirect
            redirect('login', 'location');
        }

        $user = $this->user_model->get_user(intval($user_id));

        if ($user === FALSE)
        {
            // redirect
            redirect('login', 'location');
        }
        else
        {
            // store user
            $this->user = $user;
        }
    }

    /**
     *
     * @param int $course_id
     * @return course_record
     */
    public function _get_course_or_default($course_id, $user_id = 0)
    {
        if ($user_id == 0)
        {
            $user_id = $this->user->user_id;
        }

        $course = $this->course_model->get_course($course_id);

        if ($course == FALSE)
        {
            $course = $this->user_model->get_default_course($user_id);
        }
        if ($course == FALSE)
        {
            // Could not find course so we return a dummy object.
            $course = (object) 'Kunde ej hitta kurs';
            $course->course_id = -1;
            $course->course_name = "N/A";
        }
        return $course;
    }

}

// End of file My_Controller.php
// Location: ./core/My_Controller.php
