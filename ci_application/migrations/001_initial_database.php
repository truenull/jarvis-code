<?php
/**
 * This file contains the migration to the initial db version.
 *
 * PHP version 5
 *
 * @category Migrations
 * @package  Database
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Initializes the database.
 *
 * @category Migrations
 * @package  Database
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Migration_initial_database extends CI_Migration
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

    }

    /**
     * Updates to this version.
     * 
     * @return view Register form
     */
    public function up()
    {
        $sql = file_get_contents(APPPATH . 'sql/initial_create.sql');
        $sql = explode(';', $sql);

        $this->db->trans_start();
        foreach ($sql as $query)
        {
            if (trim($query) !== '')
            {
                $this->db->query($query . ';');
            }
        }
        $this->db->trans_complete();
        $this->db->insert('migrations', array('version' => 1));
        return $this->db->trans_status();
    }

    /**
     * Undos the update to this version.
     *
     * @return view File list page
     */
    public function down()
    {
        $this->dbforge->drop_table('ci_session');
        $this->dbforge->drop_table('admins');
        $this->dbforge->drop_table('student_assignments');
        $this->dbforge->drop_table('assignment_comment');
        $this->dbforge->drop_table('student_files');
        $this->dbforge->drop_table('registrations');
        $this->dbforge->drop_table('reset_tokens');
        $this->dbforge->drop_table('assignment_files');
        $this->dbforge->drop_table('assignments');
        $this->dbforge->drop_table('courses');
        $this->dbforge->drop_table('users');
    }

}

// End of file 001_initial_database.php
// Location: ./migrations/001_initial_database.php