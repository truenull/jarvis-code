<?php
/**
 * This file contains the migration to db version 2.
 *
 * PHP version 5
 *
 * @category Migrations
 * @package  Database
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Adds a boolean field that specifies if the file can't be viewed.
 *
 * @category Migrations
 * @package  Database
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Migration_add_download_field extends CI_Migration
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

    }

    /**
     * Updates to this version.
     * 
     * @return view Register form
     */
    public function up()
    {
        $this->db->trans_start();

        $add_dowload = "ALTER TABLE `assignment_files` ADD `download_only` BOOLEAN NULL;";
        $this->db->simple_query($add_dowload);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Undos the update to this version.
     *
     * @return view File list page
     */
    public function down()
    {
        $this->db->trans_start();

        $remove_dowload = "ALTER TABLE `assignment_files` DROP `download_only`;";
        $this->db->simple_query($remove_dowload);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

}

// End of file 002_add_download_file.php
// Location: ./migrations/002_add_download_file.php