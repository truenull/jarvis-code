<?php
/**
 * This file lets you define "hooks" to extend CI without hacking the core files
 *
 * PHP version 5
 *
 * @category Config
 * @package  Hooks
 * @author   CodeIgniter <nothing@invalid.com>
 * @license  CodeIgniter license
 * @link     http://ellislab.com/codeigniter/user-guide/general/hooks.html
 * */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$hook['display_override'] = array(
    'class'    => 'DisplayHook',
    'function' => 'captureOutput',
    'filename' => 'DisplayHooks.php',
    'filepath' => 'hooks'
);

// End of file hooks.php
// Location: ./config/hooks.php
