<?php
/**
 * This file lets you define db settings.
 *
 * EMAIL SETTINGS
 *
 *
 *
 * PHP version 5
 *
 * @category Config
 * @package  Constants
 * @author   CodeIgniter <nothing@invalid.com>
 * @license  CodeIgniter license
 * @link     none
 * */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['useragent']      = 'Jarvis'; // The "user agent".
$config['protocol']       = 'mail'; // mail, sendmail, or smtp	The mail sending protocol.
$config['mailpath']       = '/usr/sbin/sendmail'; // The server path to Sendmail.
$config['smtp_host']      = '';
// SMTP Server Address. If you have a server that requires SSL use ssl://hostname
// Note: this requires your server to have OpenSSL
$config['smtp_user']      = ''; // SMTP Username.
$config['smtp_pass']      = ''; // SMTP Password.
$config['smtp_port']      = 25; // SMTP Port.
$config['smtp_timeout']   = 5; // SMTP Timeout (in seconds).
$config['wordwrap']       = TRUE; // TRUE or FALSE (boolean) - Enable word-wrap.
$config['wrapchars']      = 76; // Character count to wrap at.
$config['mailtype']         = 'html'; // text or html - Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
$config['charset']        = 'utf-8'; //	utf-8		Character set (utf-8, iso-8859-1, etc.).
$config['validate']       = FALSE; //	FALSE	TRUE or FALSE (boolean)	Whether to validate the email address.
$config['priority']       = 3; //	3	1, 2, 3, 4, 5	Email Priority. 1 = highest. 5 = lowest. 3 = normal.
$config['crlf']           = "\r\n"; // 	\n 	"\r\n" or "\n" or "\r" 	Newline character. (Use "\r\n" to comply with RFC 822).
$config['newline']        = "\r\n"; //	\n 	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
$config['bcc_batch_mode'] = TRUE; //	FALSE	TRUE or FALSE (boolean)	Enable BCC Batch Mode.
$config['bcc_batch_size'] = 80; //
$config['system_from']    = 'no-reply@host';
$config['system_from_name'] = 'no-reply';

// End of file email.sample.php
// Location: ./config/email.sample.php