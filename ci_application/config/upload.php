<?php
/**
 * Upload config file for the SysUt site
 *
 * PHP version 5
 *
 * @category Configuration
 * @package  Upload
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     http://www.ellislab.com/codeigniter/user-guide/libraries/file_uploading.html
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Convert PHP ini max filesize to kilobytes.
$size = trim(trim(strtolower(ini_get('upload_max_filesize'))), 'b');
$last = $size[\strlen($size) - 1];
$intSize = intval(substr($size, 0, \strlen($size) - 1));
switch ($last) {
    case 'g':
        $intSize *= 1024;
    case 'm':
        $intSize *= 1024;
}

$config['upload_path']       = APPPATH . '../uploads/';
$config['max_size']          = $intSize;
$config['allowed_types'] = '*';

// We want to store a filename that is not an int
$config['encrypt_name'] = TRUE;
$config['max_filename'] = 255;

// End of file upload.php
// Location: ./ci_application/config/upload.php