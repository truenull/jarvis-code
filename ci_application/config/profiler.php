<?php  
/**
 * -------------------------------------------------------------------------
 * Profiler Sections
 * -------------------------------------------------------------------------
 * This file lets you determine whether or not various sections of Profiler
 * data are displayed when the Profiler is enabled.
 * Please see the user guide for info:
 *
 *  PHP version 5
 *
 * @category Config
 * @package  Profiler
 * @author   John Doe <john.doe@example.com>
 * @license  http://ellislab.com/codeigniter/user-guide/license.html CodeIgniter
 * @link     http://codeigniter.com/user_guide/general/profiling.html
 * */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


// End of file profiler.php
// Location: ./config/profiler.php