<?php

/**
 * This file contains the model for the permission database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Identifiers for permission levels
 * @property int student
 * @property int limited_teacher
 * @property int teacher
 * @property int admin
 */
class access_level
{
    const student         = 10;
    const limited_teacher = 20;
    const teacher         = 30;
    const admin           = 40;
}

/**
 * Permissions for courses, assignments and files.
 *
 * @category Models
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://None Proprietary
 * @link     name
 */
class permission_model extends CI_Model
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returns true if the user is an administrator.
     * @param type $user_id
     * @return bool If the user has admin access
     */
    public function is_admin($user_id)
    {
        $query = $this->db->get_where(
                'admins', array('admin_id' => intval($user_id)), 1);
        return $query->num_rows() > 0;
    }

    /**
     * Returns true if the user has a teacher registration to the course or is an admin.
     * @param int $user_id
     * @param int $course_id
     * @return bool If the user has teacher access
     */
    public function is_teacher($user_id, $course_id)
    {
        if (!$this->is_admin($user_id))
        {
            $where = array(
                'user_id'   => intval($user_id),
                'course_id'    => intval($course_id),
                'access_level' => access_level::teacher
            );
            $query = $this->db->get_where('registrations', $where, 1);
            return $query->num_rows() > 0;
        }

        return true;
    }

    /**
     * Returns true if the user has a limited teacher registration to the course.
     * @param int $user_id
     * @param int $course_id
     * @return bool If the user has limited teacher access.
     */
    public function is_limited_teacher($user_id, $course_id)
    {

            $where = array(
                'user_id'   => intval($user_id),
                'course_id'    => intval($course_id),
            'access_level' => access_level::limited_teacher
        );
            $query = $this->db->get_where('registrations', $where, 1);
            return $query->num_rows() > 0;
    }


    /**
     * Returns true if the user has a student registration to the course.
     * @param int $user_id
     * @param int $course_id
     * @return bool If the user has student access.
     */
    public function is_student($user_id, $course_id)
    {
        $where = array(
            'user_id'   => intval($user_id),
            'course_id'    => intval($course_id),
            'access_level' => access_level::student
        );
        $query = $this->db->get_where('registrations', $where, 1);
        return $query->num_rows() > 0;
    }

}

// End of file permission_model.php
// Location: ./model/permission_model.php