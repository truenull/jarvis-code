<?php
/**
 * This file contains the model for the user database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Results from user_model.
 * Dummy class for autocompletition only
 *
 * @property int $user_id
 * @property string $user_name
 * @property string $email
 * @property string $password_hash
 * @property string $last_login
 *
 *
 * @category Models
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://None Proprietary
 * @link     name
 */
class user_record
{

}

/**
 * Results from user_model.
 * Dummy class for autocompletition only
 *
 * @property int $user_id
 * @property string $reset_token
 * @property int $time
 *
 * @category Models
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://None Proprietary
 * @link     name
 */
class token_record
{

}

/**
 * Create-Retrieve-Update-Delete operations for users.
 *
 * @category Models
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://None Proprietary
 * @link     name
 */
 class user_model extends CI_Model
{
    const reset_token_max_age = 86400; // 24 hours as seconds
    const max_pass_length     = 2048;
    const min_pass_length     = 6;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->helper('string_helper');
    }
	    /**
     * Gets all user objects from the database.
     * 
     * @return array(User)
     */
    function get_all_users()
    {
        return $this->db->get('users');
    }

    /**
     * Gets a user objects from the database.
     *
     * @param int $user_id The id of the file to fetch.
     * 
     * @return user_record
     */
    function get_user($user_id)
    {
        $return = FALSE;
        $array  = array('user_id' => intval($user_id));

        $result = $this->db->get_where('users', $array, 1);

        if ($result->num_rows() > 0)
        {
            $return = $result->first_row();
        }

        $result->free_result();
        return $return;
    }

    /**
     * Creates a user with the information in question.
     * @param string $name
     * @param string $email
     * @param string $password
     * @return int New user ID
     */
    function create_user($name, $email, $password)
    {
        $password = $this->hash_password($password, $email);

        // Create user
        $data     = array(
            'user_name'     => $name,
            'email'           => $email,
            'password_hash' => $password
        );
        $this->db->insert('users', $data);
        $user_id = $this->db->insert_id();
        return $user_id;
    }

    /**
     * Checks if a reset token exists.
     * @param int $user_id
     * @return boolean
     */
    public function create_reset_token_by_id($user_id)
    {
        $success = TRUE;
        try
        {
            $data = array('user_id' => intval($user_id));

            $this->db->delete('reset_tokens', $data);

            $data['reset_token'] = random_string('unique');
            $data['time']        = time();

            $this->db->insert('reset_tokens', $data);
        }
        catch (Exception $e)
        {
            log_message('error',
                        $e->getFile() . ' ('
                    . $e->getLine() . ') - ' . $e->getMessage());
            $success = FALSE;
        }
        return $success;
    }

    /**
     * Convenience function for the by_id function
     * @param string $email
     * @return boolean
     */
    public function create_reset_token_by_mail($email)
    {
        $id = $this->get_user_id($email);
        return $this->create_reset_token_by_id($id);
    }

    /**
     * Creates all users in the CSV string.
     * format:
     * Name;LastName;email, another email;\n
     * Will use the first email
     * 
     * @param string $csv The contents of a csv file
     * @return array(int) A bunch of user_id
     */
    public function create_from_csv($csv)
    {
        $users    = explode("\n", $csv);
        $user_ids = array();
        foreach ($users as $user)
        {
            if (trim($user) === '')
            {
                continue;
            }
            $userArr = explode(';', $user);

            $name    = ucwords(trim($userArr[0]))
                    . ' ' . ucwords(trim($userArr[1]));
            $emails = explode(',', $userArr[2], 2);
            $email   = trim($emails[0]);

            $password = random_string('alnum', 20);
            $user_id    = $this->get_user_id($email);
            if ($user_id == 0)
            {
                $user_id = $this->create_user($name, $email, $password);
            }

            if ($user_id !== FALSE)
            {
                $user_ids[] = $user_id;
            }
        }

        return $user_ids;
    }

    /**
     * Removes tokens older than reset_token_max_age.
     */
    public function remove_expired_tokens()
    {
        $age        = user_model::reset_token_max_age;
        $conditions = array('time <' => time() - $age);
        $this->db->delete('reset_tokens', $conditions);
    }

    /**
     * Removes the reset token if it exists.
     * @param string $reset_token
     */
    public function remove_reset_token($reset_token)
    {
        $conditions = array('reset_token' => $reset_token);
        $this->db->delete('reset_tokens', $conditions);
    }

    /**
     * WARNING: Will remove user. Foreign key constraints are set to CASCADE.
     * @param int $user_id
     * @return int Affected rows.
     */
    function remove_user($user_id)
    {
        $this->db->delete('users', array('user_id' => intval($user_id)));
        return $this->db->affected_rows();
    }

    /**
     *
     * @param type $user_id
     * @param type $reset_token
     * @return bool
     */
    public function reset_token_exists($user_id, $reset_token)
    {
        $this->remove_expired_tokens();

        $request = array(
            'user_id'     => intval($user_id),
            'reset_token' => $reset_token);
        $query   = $this->db->get_where('reset_tokens', $request);
        $answer  = $query->num_rows() > 0;
        $query->free_result();
        return $answer;
    }

    /**
     * Retrieves reset token for the user in question.
     * @param int $user_id
     * @return token_record
     */
    public function get_reset_token($user_id)
    {
        $this->remove_expired_tokens();

        $answer = FALSE;

        $request = array(
            'user_id' => intval($user_id));
        $query   = $this->db->get_where('reset_tokens', $request);

        if ($query->num_rows() > 0)
        {
            $answer = $query->first_row();
        }

        $query->free_result();

        return $answer;
    }

        /**
     * Retrieves reset token for the user in question.
     * @param int $user_id
     * @return token_record
     */
    public function get_reset_token_by_token($reset_token)
    {
        $this->remove_expired_tokens();

        $answer = FALSE;

        $request = array(
            'reset_token' => $reset_token);
        $query   = $this->db->get_where('reset_tokens', $request);

        if ($query->num_rows() > 0)
        {
            $answer = $query->first_row();
        }

        $query->free_result();

        return $answer;
    }

    /**
     * Returns the user id associated with the email, or 0 if none exist;
     * @param string $email
     * @return int
     */
    public function get_user_id($email)
    {
        $return = 0;
        $array = array('email' => $email);

        $result = $this->db->get_where('users', $array, 1);

        if ($result->num_rows() > 0)
        {
            $return = intval($result->first_row()->user_id);
        }
        $result->free_result();
        return $return;
    }

    /**
     * Gets the newest course the user was registered for.
     * If the user is admin, we do not check the registration status.
     * @param int $user_id
     * @param bool $suppress_admin
     * @return course_record Or FALSE if none.
     */
    public function get_default_course($user_id, $suppress_admin = FALSE)
    {
        $course = $this->get_latest_courses($user_id, 1, $suppress_admin);
        if (count($course) <= 0)
        {
            $course = $this->course_model->get_all_courses();
        }

        return count($course) > 0? $course[0]:FALSE;
    }

    /**
     * Gets the N latest courses that the user is signed up for.
     * If the user is admin, we get the N latest courses.
     * @param type $user_id
     * @param type $amount
     * @param bool $suppress_admin If true, does not pay special consideration to admins
     */
    public function get_latest_courses($user_id, $amount = 5,
                                       $suppress_admin = FALSE)
    {
        $is_admin = $this->permission_model->is_admin($user_id);
        // If we're not suppressing admin rights and user is not admin
        if (!(!$suppress_admin && $is_admin))
        {
            // Limit to registered courses
            $this->db->join('registrations',
                            'registrations.course_id = courses.course_id');
            $this->db->where('registrations.user_id', intval($user_id));
        }
        
        $this->db->select('courses.*');
        // Oldest courses first
        $this->db->order_by('end_time', 'desc');
        $query  = $this->db->get('courses', $amount);
        $result = $query->result();
        return $result;
    }

    /**
     * Returns TRUE if the email exists in the database and the password is valid.
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function validate_login($email, $password)
    {
        $user_id = $this->get_user_id($email);
        if ($user_id === FALSE)
        {
            return FALSE;
        }

        $user = $this->get_user($user_id);
        if ($user === FALSE)
        {
            return FALSE;
        }

        // Crypt gives the salt joined with the hash in it's return
        // Return is guaranteed not to be equal to the hash on failure.
        // This is why we can do this:
        return $user->password_hash == crypt($password, $user->password_hash);
    }

    public function set_admin($user_id)
    {
        $data = array('admin_id' => intval($user_id));
        $query = $this->db->get_where('admins', $data, 1);
        if($query->num_rows() < 1)
        {
            $this->db->insert('admins', $data);
        }
    }

    public function set_not_admin($user_id)
    {
        $data = array('admin_id' => intval($user_id));
        $this->db->where( $data);
        $this->db->delete('admins');
    }

    /**
     * Sets the password for the user.
     * @param string $password
     * @param int $user_id
     * @return boolean
     */
    public function set_password($password, $user_id)
    {
        $user = $this->get_user($user_id);
        if ($user === FALSE)
        {
            return FALSE;
        }

        $hashed = $this->hash_password($password, $user->email);

        $this->db->where('user_id', intval($user_id));
        $this->db->update('users', array('password_hash' => $hashed));
        return TRUE;
    }


    public function set_name($name, $user_id)
    {
        $user = $this->get_user($user_id);
        if ($user === FALSE)
        {
            return FALSE;
        }

        $this->db->where('user_id', intval($user_id));
        $this->db->update('users', array('user_name' => $name));
        return TRUE;
    }
    
    public function set_email($email, $user_id)
    {
        $user = $this->get_user($user_id);
        if ($user === FALSE)
        {
            return FALSE;
        }

        $this->db->where('user_id', intval($user_id));
        $this->db->update('users', array('email' => $email));
        return TRUE;
    }

    /**
     * Hashes the password
     * @param string $password
     * @return string
     * @todo switch this out to use crypt() instead of our own hashing method
     */
    function hash_password($password, $email)
    {
        return crypt($password, $this->get_salt($email));
    }

    public function get_salt($email)
    {
        $enc_email = base64_encode($email);
        if (CRYPT_SHA512 == 1)
        {
            return '$6$rounds=5000$' . $enc_email . '$';
        }
        if (CRYPT_SHA256 == 1)
        {
            return '$5$rounds=5000$' . $enc_email . '$';
        }

        if (CRYPT_BLOWFISH == 1)
        {
            return '$2a$07$' . $enc_email . '$';
        }

        if (CRYPT_MD5 == 1)
        {
            // salt is limited to 12 chars total including $ bits
            return '$1$' . substr($enc_email, 0, 8) . '$';
        }

        show_error('Den installerade versionen av PHP är för gammalt för att stödja säkra hashfunktioner.');
    }
	
	
	
	/*** Eke funktioner ***/
	public function get_all_courses($user_id)
	{
		
		$result = $this->db->query("
				SELECT *
				FROM courses
				");
		return $result->result(); 
	}
	
	public function register_user_to_course($course_id,$user_id)
	{
		$this->db->select('*');
		$this->db->where('user_id', intval($user_id));
		$this->db->where('course_id', intval($course_id));
		$this->db->from('registrations');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return 0;
		}
		
		$data = array(
			'user_id' => $user_id,
			'course_id' => $course_id,
			'access_level' => 10 );
		$this->db->insert('registrations', $data);
		return 1;
	}
}

// End of file user_model.php
// Location: ./model/user_model.php