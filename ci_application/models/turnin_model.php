<?php

/**
 * This course contains the model for the turnin database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Results from turnin_model.
 * Dummy class for autocompletition only
 *
 * @property int $assignment_id
 * @property int $user_id
 * @property int $grade
 * @property int $status
 *
 */
class turnin_record
{

}

/**
 * Joined results from turnin_model.
 * Dummy class for autocompletition only
 *
 * @property int $user_id
 * @property string $user_name
 * @property string $email
 * @property string $password_hash
 * @property string $last_login
 * @property int $course_id
 * @property string $course_tag
 * @property string $course_name
 * @property string $course_description
 * @property int $end_time
 * @property boolean $mandatory
 */
class student_turnin_record extends turnin_record {

}

/**
 * Result for a students grade and status for an assignment.
 * Dummy class for autocompletition only
 *
 * @property int $grade
 * @property int $status
 */
class assignment_status_record {}

/**
 * Create-Retrieve-Update-Delete operations for student assignment turnins (grades, files).
 *
 * @category Models
 * @package  jarvis
 * @author   Projektgruppen Järv
 * @license  http://None Proprietary
 * @link     name
 */
class turnin_model extends CI_Model
{
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets the turnins for the course and student.
     * @param int $course_id
     * @param int $user_id Gets all students turnins for the course if this is zero.
     * @return array(student_turnin_record)
     */
    public function get_all_student_turnins($course_id, $user_id = 0)
    {
        //$this->db->distinct();
        // Display ID columns as aliases since otherwise they become blank
        $this->db->select('users.user_id AS user_id');
        $this->db->select('assignments.assignment_id AS assignment_id');
        $this->db->select('registrations.course_id AS course_id');
        $this->db->select('assignments.due_date AS due_date');
        $this->db->select('student_assignments.grade AS grade');
        $this->db->select('student_assignments.status AS status');

        // Columns that only exist in one table.
        $this->db->select('user_name, email, password_hash, last_login, '
                . 'assignment_number, assignment_name, assignment_desc, '
                . 'max_points, mandatory');


        $this->db->from('users');

        $this->db->join('registrations', 'users.user_id = registrations.user_id',
                                                            'inner');
        $this->db->join('assignments',
                        'assignments.course_id = registrations.course_id',
                        'RIGHT OUTER');
        $this->db->join('student_assignments',
                        'users.user_id = student_assignments.user_id '
                . ' AND assignments.assignment_id ='
                . ' student_assignments.assignment_id', 'left outer');

        $this->db->where('registrations.course_id', intval($course_id));
        $this->db->where('registrations.access_level', access_level::student);
        if ($user_id != 0)
        {
            $this->db->where('registrations.user_id', intval($user_id));
        }

        $this->db->order_by('assignments.assignment_number');
        $this->db->order_by('assignments.assignment_id');
        $query  = $this->db->get();
        $result = $query->result();
        $query->free_result();
        //die(print_r($this->db->last_query()));
        return $result;
    }

    /**
     * Gets array containing grade and status for specific user and assignment from the database.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @return assignment_status_record FALSE if no matches
     */
    function get_assignment_status($user_id, $assignment_id)
    {
        $result = FALSE;

        //$this->db->select('*');
        $this->db->select('grade, status');
        $this->db->from('student_assignments');
        $this->db->where('user_id', intval($user_id));
        $this->db->where('assignment_id', intval($assignment_id));
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            $result = $query->first_row();
        }
        else
        {
            $result = FALSE;
        }

        $query->free_result();

        return $result;
    }

    /**
     * Gets object containing commenter_id and comment for specific user and assignment from the database.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @return object(assignment_comments) empty object if no matches
     */
    function get_assignment_comments($user_id, $assignment_id)
    {

        //$this->db->select('*');
        $this->db->select('users.user_name AS commenter_name');
        $this->db->select('assignment_comment.comment AS comment');
        $this->db->from('assignment_comment');
        $this->db->join('users',
                        'assignment_comment.commenter_id = users.user_id',
                        'left');
        $this->db->where('assignment_comment.user_id', intval($user_id));
        $this->db->where('assignment_comment.assignment_id',
                         intval($assignment_id));
        $this->db->order_by('comment_id', 'asc');
        $query = $this->db->get();

        $result = $query->result();
        $query->free_result();

        return $result;
    }

    /**
     *
     * @param type $assignment_id
     * @return assignment_file_record
     */
    public function get_all_assignment_files($assignment_id)
    {
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->from('assignment_files');
        $query  = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    /**
     * Gets all the files for a particular assignment.
     * @param in $assignment_id
     */
    function get_all_student_files($assignment_id)
    {
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->from('assignment_files');
        $this->db->join('student_files',
        'assignment_files.assignment_file_id = student_files.assignment_file_id',
                        'inner');
        $this->db->select('assignment_files.assignment_file_id AS assignment_file_id');
        $this->db->select('assignment_id, file_name, user_id, upload_date');
        $query  = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    /**
     * Sets assignment status to turned in if all files have db records.
     * @param type $user_id
     * @param type $assignment_id
     */
    public function eval_assignment_status($user_id, $assignment_id)
    {
        $user_id = intval($user_id);
        $assignment_id = intval($assignment_id);

        $this->db->where('assignment_id', $assignment_id);
        $files = $this->db->count_all_results('assignment_files');


        $this->db->where('assignment_id', $assignment_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('student_files.upload_date !=', 0);
        $this->db->join('student_files',
        'assignment_files.assignment_file_id = student_files.assignment_file_id',
                        'inner');
        $turnins = $this->db->count_all_results('assignment_files');

        
        $this->db->where('assignment_id', $assignment_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('commenter_id', $user_id);
        $comments = $this->db->count_all_results('assignment_comment');

        if($turnins === $files && $turnins != FALSE)
        {
            $this->set_status($user_id, $assignment_id, assignment_status::under_grading);
        }
        else if ($turnins == FALSE && $comments > 0)
        {
            $this->set_status($user_id, $assignment_id, assignment_status::under_grading);
        }
        else
        {
            $this->set_status($user_id, $assignment_id, assignment_status::not_turned_in);
        }
        return FALSE;
    }

    /**
     * Removes the students turned in file, if any.
     * @param int $user_id
     * @param int $assignment_file_id
     * @param string $upload_path
     */
    function delete_student_file($user_id,
                                 $assignment_file_id,
                                 $upload_path = FALSE)
    {
        // Remove file from disk
        $this->load->model('upload_model');
        $file = $this->upload_model->get_student_file_path($user_id, $assignment_file_id);
        if (file_exists($file))
        {
            unlink($file);
        }

        $hl_file = $this->upload_model->get_student_file_path($user_id, $assignment_file_id, TRUE);
        if (file_exists($hl_file))
        {
            unlink($hl_file);
        }

        // Cleanup the database
        $this->db->where('user_id', intval($user_id));
        $this->db->where('user_id', intval($assignment_file_id));
        $this->db->delete('student_files');
    }

    /**
     * Insert comment into database.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @param int $commenter_id
     * @param int $comment
     */
    function create_comment($user_id, $assignment_id, $commenter_id, $comment)
    {
        $data = array(
            'user_id'       => $user_id,
            'assignment_id' => $assignment_id,
            'commenter_id'  => $commenter_id,
            'comment'       => $comment
        );
        $this->db->insert('assignment_comment', $data);
    }

    /**
     * Insert student assignment.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @return int Affected rows
     */
    function create_student_assignment($user_id, $assignment_id)
    {
        // Check to see if a row already exist with the corresponding user_id
        // and assignment_id. If it does we return 0.
        $this->db->select('*');
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->where('user_id', intval($user_id));
        $this->db->from('student_assignments');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            return 0;
        }
        
        // Insert a new row into the database with the corresponding user_id
        // and assignment_id. Return 1.
        
        $data = array(
            'user_id'       => $user_id,
            'assignment_id' => $assignment_id,
            'grade'         => 0,
            'status'        => 0
        );
        $this->db->insert('student_assignments', $data);
        return 1;
    }

    /**
     * Set grade of student assignment.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @param int $grade
     * @return int Affected rows
     */
    function set_grade($user_id, $assignment_id, $grade)
    {
        // Make sure the user has permission to grade.
        $has_access = false;
        $course     = $this->assignment_model->get_assignment($assignment_id)->course_id;
        if ($this->permission_model->is_limited_teacher($this->user->user_id,
                                                        $course) || $this->permission_model->is_teacher($this->user->user_id,
                                                                                                        $course) || $this->permission_model->is_admin($this->user->user_id))
        {
            $has_access = true;
        }
        if (!$has_access)
        {
            show_error('Du har inte tillgång till denna funktion.');
        }

        // Create student_assignment row if it doesn't already exist.
        $this->turnin_model->create_student_assignment($user_id, $assignment_id);

        // Set grade.
        $data = array(
            'grade' => $grade,
        );
        $this->db->where('user_id', intval($user_id));
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('student_assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Set status of student assignment.
     *
     * @param int $user_id
     * @param int $assignment_id
     * @param int $status
     * @return int Affected rows
     */
    function set_status($user_id, $assignment_id, $status)
    {
        // Make sure the user has permission to grade.
        $has_access = false;
        $course     = $this->assignment_model->get_assignment($assignment_id)->course_id;
        if ($this->permission_model->is_limited_teacher(
                    $this->user->user_id,
                    $course) 
                || $this->permission_model->is_teacher(
                        $this->user->user_id,
                        $course)
                || $this->permission_model->is_admin($this->user->user_id))
        {
            $has_access = true;
        }
        elseif( $this->user->user_id == $user_id
                && ( $status === assignment_status::not_turned_in
                || $status === assignment_status::under_grading ))
        {
            // User has access if he is himself and turning in or out
            $has_access = true;
        }
        
        if (!$has_access)
        {
            show_error('Du har inte tillgång till denna funktion.');
        }

        // Create student_assignment row if it doesn't already exist.
        $this->turnin_model->create_student_assignment($user_id, $assignment_id);

        // Set status.
        $data = array(
            'status' => $status,
        );
        $this->db->where('user_id', intval($user_id));
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('student_assignments', $data);

        return $this->db->affected_rows();
    }

}

// End of file turnin_model.php
// Location: ./model/turnin_model.php