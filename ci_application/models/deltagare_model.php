<?php
/**
 * This course contains the model for the course database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Jonathan Ekelund
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class deltagare_model extends CI_Model
{
  /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }
	

    //Returns all members for the course
    function get_members($course_id)
    {
        $result = $this->db->query("
                        SELECT users.user_name,users.user_id,users.email,registrations.course_id
                        FROM `users`
                        join registrations
                        ON users.user_id=registrations.user_id
                        where registrations.access_level = '10'
                        and registrations.course_id='".$course_id."'
                        ");
        return $result->result();
    }


    /**
     * Get the total accumulated points for a specific user and course.
     * @param string $user_id
     * @param string $course_id
     * @return int Total accumulated points
     */
    function get_accumulated_points($course_id, $user_id = 0)
    {
        $course_id = intval($course_id);
        $user_id = intval($user_id);
        $is_student = $this->permission_model->is_student($this->session->userdata['user_id'], $course_id);
        if ($is_student)
        {
            $user_id = $this->session->userdata['user_id'];
        }

        $this->db->distinct('student_assignments.user_id');
        $this->db->select('student_assignments.user_id');
        $this->db->select_sum('grade');
        $this->db->from('student_assignments');
        $this->db->join('assignments',
                'student_assignments.assignment_id = assignments.assignment_id',
                'inner');
        if ($user_id !== 0 || $is_student)
        {
            $this->db->where('student_assignments.user_id', $user_id);
        }
        $this->db->where('course_id', $course_id);
        $this->db->group_by('student_assignments.user_id');
        $query = $this->db->get();
        
        $result = $query->result();
        $query->free_result();
        
        return $result;
    }
}

// End of file deltagare_model.php
// Location: ./model/deltagare_model.php
