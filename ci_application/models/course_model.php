<?php
/**
 * This course contains the model for the course database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Results from course_model.
 * Dummy class for autocompletition only
 *
 * @property int $course_id
 * @property string $course_tag
 * @property string $course_name
 * @property string $course_description
 * @property int $end_time
 *
 *
 * @category Models
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://None Proprietary
 * @link     name
 */
class course_record
{

}

/**
 * Create-Retrieve-Update-Delete operations for courses.
 *
 * @category Models
 * @package  jarvis
 * @author   Projektgruppen Järv
 * @license  http://None Proprietary
 * @link     name
 */
class course_model extends CI_Model
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Creates a course with the attributes in question.
     * @param string $tag
     * @param string $name
     * @param string $desc
     * @param int $end_time
     * @return int New user ID
     */
    function create_course($tag, $name, $desc, $end_time)
    {
        $data      = array(
            'course_tag'         => $tag,
            'course_name'        => $name,
            'course_description' => $desc,
            'end_time'           => $end_time
        );
        $this->db->insert('courses', $data);
        $course_id = $this->db->insert_id();

        return $course_id;
    }

    /**
     * WARNING: This will delete the course. Deletes to courses will CASCADE to
     * EVERYTHING associated with the course (now including turned in files).
     * @param int $course_id
     * @return int Affected rows
     */
    function delete_course($course_id)
    {
        $this->load->model('assignment_model');
        $this->load->model('turnin_model');

        // Delete all the files for the course's assignments
        $assignments = $this->assignment_model->get_assignments($course_id);
        /* @var $assigment assignment_record  */
        foreach ($assignments as $assignment)
        {
            $files = $this->turnin_model->get_all_student_files($assignment->assignment_id);
            foreach ($files as $stud_file)
            {
                $this->turnin_model->delete_student_file(
                    $stud_file->user_id, $stud_file->assignment_file_id
                );
            }
        }

        // Finally, remove all traces from the database.
        $where = array(
            'course_id' => intval($course_id)
        );
        $this->db->delete('courses', $where);
        return $this->db->affected_rows();
    }

    /**
     * Gets all course objects from the database.
     *
     * @param int $user_id Get all courses for user.
     * @return array(course_record) Empty if no matches
     */
    function get_all_courses($user_id = 0)
    {

        if ($user_id != 0 && !$this->permission_model->is_admin($user_id))
        {
            $this->db->where('user_id', intval($user_id));
            $this->db->join('registrations',
                        'courses.course_id = registrations.course_id');
        }
        $this->db->select('*');
        $query  = $this->db->get('courses');
        $result = $query->result();
        $query->free_result();

        return $result;
    }

    /**
     * Gets a course objects from the database.
     *
     * @param int $course_id The id of the file to fetch.
     * @return course_record Or false if none exist.
     */
    function get_course($course_id)
    {
        $return = FALSE;
        $array  = array('course_id' => intval($course_id));

        $result = $this->db->get_where('courses', $array, 1);

        if ($result->num_rows() > 0)
        {
            $return = $result->first_row();
        }

        $result->free_result();
        return $return;
    }



    /**
     * Changes the description of the course.
     * @param string $desc
     * @param int $course_id
     * @return int Affected rows
     */
    function set_desc($desc, $course_id)
    {
        $data = array(
            'course_description' => $desc
        );
        $this->db->where('course_id', intval($course_id));
        $this->db->update('courses', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the end time of the course.
     * @param int $end_time
     * @param int $course_id
     * @return type
     */
    function set_end_time($end_time, $course_id)
    {
        $data = array(
            'end_time' => intval($end_time)
        );
        $this->db->where('course_id', intval($course_id));
        $this->db->update('courses', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the name of the course.
     * @param type $name
     * @param type $course_id
     * @return int Affected rows
     */
    function set_name($name, $course_id)
    {
       $data      = array(
            'course_name' => $name,
        );
        $this->db->where('course_id', intval($course_id));
        $this->db->update('courses', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the tag of the course.
     * @param string $tag
     * @param int $course_id
     * @return int Affected rows
     */
    function set_tag($tag, $course_id)
    {
        $data = array(
            'course_tag' => $tag,
        );
        $this->db->where('course_id', intval($course_id));
        $this->db->update('courses', $data);

        return $this->db->affected_rows();
    }

    /**
     * Registera a user to a course with the access level specified
     * @param int $course_id
     * @param int $user_id
     * @param int $access_level An integer constant from the access_level class.
     * @return int Affected rows
     */
    private function register($course_id, $user_id, $access_level)
    {
        // an admin already have access
        if ($this->permission_model->is_admin($user_id))
        {
            return 1;
        }

        $data = array(
            'course_id'    => $course_id,
            'user_id'      => $user_id,
            'access_level' => $access_level
        );

        $this->db->insert('registrations', $data);

        return $this->db->affected_rows();
    }

    /**
     * Registers a teacher to a course.
     * @param type $course_id
     * @param type $user_id
     */
    function register_teacher($course_id, $user_id)
    {
        $this->register($course_id, $user_id, access_level::teacher);
    }

    /**
     * Registers a student to a course.
     * @param type $course_id
     * @param type $user_id
     */
    function register_student($course_id, $user_id)
    {
        $this->register($course_id, $user_id, access_level::student);
    }

    /**
     * Registers a limited teacher to a course.
     * @param type $course_id
     * @param type $user_id
     */
    function register_limited($course_id, $user_id)
    {
        $this->register($course_id, $user_id, access_level::limited_teacher);
    }

    /**
     * Deassociates a user from a course. Remember to delete turnins & comments.
     * @param int $user_id
     * @param int $course_id
     * @return int Affected rows
     */
    function unregister($user_id, $course_id)
    {
        $where = array(
            'user_id'   => intval($user_id),
            'course_id' => intval($course_id)
        );
        $this->db->delete('registrations', $where, 1);

        return $this->db->affected_rows();
    }

}

// End of file course_model.php
// Location: ./model/course_model.php
