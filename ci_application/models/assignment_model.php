<?php

/**
 * This course contains the model for the assignment database of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Results from user_model.
 * Dummy class for autocompletition only
 *
 * @property int $assignment_file_id
 * @property int $assignment_id
 * @property string $file_name
 * @property boolean $download_only
 *
 */
class assignment_file_record
{

}

/**
 * Results from assignment_model.
 * Dummy class for autocompletition only
 *
 * @property int $assignment_id
 * @property int $assignment_number
 * @property string $assignment_name
 * @property string $assignment_desc
 * @property int $course_id
 * @property int $due_date
 * @property int $max_points
 * @property boolean $mandatory
 *
 */
class assignment_record
{

}

/**
 *
 * @property-read int approved Value when approved and graded.
 * @property-read int failed Value when no longer able to be turned in.
 * @property-read int under_grading Value when completely turned in but not yet graded.
 * @property-read int not_turned_in Value when not turned in.
 *
 */
class assignment_status
{
    const approved = 30;
    const failed = 20;
    const under_grading = 10;
    const not_turned_in = 0;
}

/**
 * Create-Retrieve-Update-Delete operations for assignments and their files.
 *
 * @category Models
 * @package  jarvis
 * @author   Projektgruppen Järv
 * @license  http://None Proprietary
 * @link     name
 */
class assignment_model extends CI_Model
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * 
     * @param int $course_id
     * @param int $number
     * @param string $name
     * @param string $desc
     * @param int $due_date
     * @param int $max_points
     * @return int Id of the assignment
     */
    function create_assignment($course_id, $number, $name, $desc, $due_date,
                               $max_points, $mandatory)
    {
        $data      = array(
            'assignment_name'    => $name,
            'course_id'          => $course_id,
            'assignment_number' => $number,
            'assignment_desc'    => $desc,
            'due_date'           => $due_date,
            'max_points'        => $max_points,
            'mandatory'         => intval($mandatory) // turn it into a real bool
        );

        $this->db->insert('assignments', $data);
        $assignment_id = $this->db->insert_id();

        return $assignment_id;
    }

    /**
     * WARNING: This will delete the assignment. Deletes to assignment will
     * CASCADE to EVERYTHING with a FK from assignments.
     * @param int $assignment_id
     * @return int Affected rows
     */
    function delete_assignment($assignment_id)
    {
        $where = array(
            'assignment_id' => intval($assignment_id)
        );
        $this->db->delete('assignments', $where);
        return $this->db->affected_rows();
    }

    /**
     * Gets all assignment objects from the database.
     *
     * @param int $course_id Get all assignments for course.
     * @return array(assignment_record) Empty if no matches
     */
    function get_assignments($course_id)
    {
        $this->db->select('*');
        $this->db->from('assignments');
        $this->db->where('course_id', intval($course_id));
        $query  = $this->db->get();
        $result = $query->result();
        $query->free_result();

        return $result;
    }

    /**
     *
     * @param int $assignment_id
     * @return assignment_record Or false if none is found
     */
    function get_assignment($assignment_id)
    {
        $result = FALSE;

        if ($assignment_id === 0)
        {
            return $result;
        }

        $array = array('assignment_id' => intval($assignment_id));
        $query = $this->db->get_where('assignments', $array);

        if ($query->num_rows() > 0)
        {
            $result = $query->first_row();
        }

        $query->free_result();

        return $result;
    }

    /**
     * Gets a assignment file objects from the database.
     *
     * @param int $assignment_id The FK id of the files to fetch.
     * @return array(assignment_file_record) Empty array if none exist.
     */
    function get_assignment_files($assignment_id)
    {
        $array  = array('assignment_id' => intval($assignment_id));

        $query = $this->db->get_where('assignment_files', $array);

        $result = $query->result();
        $query->free_result();

        return $result;
    }

    /**
     * Gets the assignment file.
     * @param type $assignment_file_id
     * @return assignment_file_record Or FALSE if none exist.
     */
    function get_assignment_file($assignment_file_id)
    {
        $query = $this->db->get_where('assignment_files',
                                      array(
            'assignment_file_id' => intval($assignment_file_id)
                )
        );

        if ($query->num_rows() > 0)
        {
            $file = $query->first_row();
            $query->free_result();
            return $file;
        }

        return FALSE;
    }

    /**
     * Returns the maximum assignment number for the course.
     * @param int $course_id
     * @return int
     */
    public function get_max_assignment_number($course_id)
    {
        $course_id = intval($course_id);
        $result = 0;
        $this->db->where('course_id', $course_id);
        $this->db->order_by('assignment_number', 'desc');
        $query = $this->db->get('assignments');
        if($query->num_rows() > 0)
        {
            $max_assignment = $query->row();
            $result = intval($max_assignment->assignment_number);
        }
        $query->free_result();
        return $result;
    }

    public function get_point_sum($course_id)
    {
        $this->db->select_sum('max_points');
        $this->db->where('course_id', $course_id);
        $query = $this->db->get('assignments');
        if($query->num_rows())
        {
            $row = $query->row();
            $answer  = $row->max_points;
        }
        else
        {
            $answer = 0;
        }
        
        return $answer;
    }

    function create_file($file_name, $assignment_id, $download_only)
    {
        $this->db->insert('assignment_files',
                          array(
            'assignment_id' => intval($assignment_id),
            'file_name'     => $file_name,
            'download_only' => $download_only
        ));
    }

    /**
     * Adds files thare are not in $files to the database.
     * @param array(string) $files
     * @param int $assignment_id
     */
    function set_assignment_files($files, $assignment_id)
    {
        $assignment_id = intval($assignment_id);
        $current_files = $this->get_assignment_files($assignment_id);

        foreach ($files as $file_name)
        {
            $file_name = trim($file_name);
            $download_only = substr($file_name,0,1) == '+';
            if($download_only)
            {
                $file_name = substr($file_name,1);
            }

            if ($file_name != '')
            {
                $add_file = true;
                foreach ($current_files as $current_file)
                {
                    /* @var $current_file  assignment_file_record*/
                    if ($current_file->file_name == $file_name)
                    {
                        $add_file = false;
                        if($current_file->download_only != $download_only)
                        {
                            $this->set_download_only(
                                    $current_file->assignment_file_id,
                                    $download_only);
                        }
                        break;
                    }

                }

                if ($add_file)
                {
                    $this->create_file($file_name, $assignment_id, $download_only);
                }
            }
        }
    }


    public function set_download_only($assignment_file_id, $download_only)
    {
        $this->db->where('assignment_file_id', $assignment_file_id);
        $this->db->update('assignment_files', array('download_only' => $download_only));
    }

    /**
     * Changes the description of the assignment.
     * @param string $desc
     * @param int $assignment_id
     * @return int Affected rows
     */
    function set_desc($desc, $assignment_id)
    {
        $data = array(
            'assignment_desc' => $desc
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the due date of the assignment.
     * @param int $due_date
     * @param int $assignment_id
     * @return type
     */
    function set_due_date($due_date, $assignment_id)
    {
        $data = array(
            'due_date' => intval($due_date)
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the maximum points of the assignment.
     * @param int $due_date
     * @param int $assignment_id
     * @return type
     */
    function set_max_points($points, $assignment_id)
    {
        $data = array(
            'max_points' => intval($points)
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Changes the name of the assignment.
     * @param type $name
     * @param type $assignment_id
     * @return int Affected rows
     */
    function set_name($name, $assignment_id)
    {
        $data = array(
            'assignment_name' => $name,
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Sets the assignments number to the specified value.
     * @param int $number
     * @param int $assignment_id
     * @return type
     */
    function set_number($number, $assignment_id)
    {
        $data = array(
            'assignment_number' => intval($number)
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Sets the assignments number to the specified value.
     * @param bool $mandatory
     * @param int $assignment_id
     * @return type
     */
    function set_mandatory($mandatory, $assignment_id)
    {
        $data = array(
            'mandatory' => intval($mandatory)
        );
        $this->db->where('assignment_id', intval($assignment_id));
        $this->db->update('assignments', $data);

        return $this->db->affected_rows();
    }

    /**
     * Adds a file to an assignment.
     * @param type $assignment_id
     * @param type $file_name
     * @return int Id of the file.
     */
    private function add_file($assignment_id, $file_name)
    {
        $data = array(
            'assignment_id' => $assignment_id,
            'file_name'     => $file_name,
        );

        $this->db->insert('assignment_files', $data);

        return $this->db->insert_id();
    }

    /**
     *
     * @param type $assignment_file_id
     * @return type
     */
    function remove_file($assignment_file_id)
    {
        $where = array(
            'user_id' => intval($assignment_file_id)
        );
        $this->db->delete('registrations', $where, 1);

        return $this->db->affected_rows();
    }
}

// End of file assignment_model.php
// Location: ./model/assignment_model.php