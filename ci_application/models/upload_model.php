<?php

/**
 * This file contains the model for the file upload logic.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Model containing the upload file logic.
 *
 * @property-read HL_ENDING The file ending for highlighted files.
 * 
 * @category Models
 * @package  jarvis
 * @author   Projektgruppen Järv
 * @license  http://None Proprietary
 * @link     name
 */
class upload_model extends CI_Model
{
    const HL_ENDING = '.highlighted';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
    }

        /**
     *
     * @param int $user_id
     * @param int $assignment_file_id
     * @param bool $highlighted
     * @param string $upload_path
     * @return string
     */
    function get_student_file_path($user_id, $assignment_file_id,
                                    $highlighted = FALSE, $upload_path = FALSE)
    {        
        if ($upload_path === FALSE)
        {
            $this->config->load('upload');
            $upload_path = realpath($this->config->item('upload_path'));
        }
        
        $file = $upload_path . DIRECTORY_SEPARATOR
                . intval($user_id) . '-' . intval($assignment_file_id)
                . (($highlighted != TRUE) ? '' : upload_model::HL_ENDING) ;
        
        return $file;
    }
    
    /**
     * Saves the file to the database
     * @param int $user_id
     * @param int $assignment_file_id
     * @return boolean
     */
    public function save_file($user_id, $assignment_file_id)
    {
        $user_id = intval($user_id);
        $assignment_file_id = intval($assignment_file_id);
        
        if (!$this->upload->do_upload('file'.$assignment_file_id))
        {
            $this->upload->display_errors();
            $error = array($this->upload->display_errors('', "\n"));

            show_error($error);
        }
        else
        {
            $data = $this->upload->data();
            
            $save_path = $this->get_student_file_path($user_id, $assignment_file_id);

            // Clear hl cache
            unlink($save_path . upload_model::HL_ENDING);

            $moved = rename($data['full_path'], $save_path);

            if(!$moved)
            {
                return FALSE;
            }

            return $this->update_file_database($user_id, $assignment_file_id);
        }
    }

    /**
     *
     * @param int $user_id
     * @param int $assignment_file_id
     * @return boolean
     */
    public function update_file_database($user_id, $assignment_file_id)
    {
        $user_id = intval($user_id);
        $assignment_file_id = intval($assignment_file_id);
        
        $this->db->from('student_files');
        $this->db->where('user_id', $user_id);
        $this->db->where('assignment_file_id', $assignment_file_id);
        $count = $this->db->count_all_results();

        $data = array(
            'upload_date' => time()
        );

        if($count < 1)
        {
            $data['user_id'] = $user_id;
            $data['assignment_file_id'] = $assignment_file_id;
            $this->db->insert('student_files', $data);
        }
        else
        {
            $this->db->where('user_id', $user_id);
            $this->db->where('assignment_file_id', $assignment_file_id);
            $this->db->update('student_files', $data);
        }
        
        return $this->db->affected_rows() === 1;
    }
}

// End of file upload_model.php
// Location: ./model/upload_model.php