<?php
/**
 * A config hook that disables output.
 *
 * PHP version 5
 *
 * @category Config
 * @package  PHPUnit
 * @author   John Doe <john.doe@example.com>
 * @license  http://NONE None
 * @link     http://NONE description
 */

/**
 * Hook that disables output if in testing mode.
 * 
 * @category Config
 * @package  PHPUnit
 * @author   John Doe <john.doe@example.com>
 * @license  http://URL name
 * @link     URL description
 */
class DisplayHook
{
    public function __construct()
    {
    }

    /**
     * Disables output if ENVIRONMENT is testing.
     * 
     * @return void Nothing
     */
    public function captureOutput()
    {
        $this->CI = & get_instance();
        $output = $this->CI->output->get_output();
        if (ENVIRONMENT != 'testing') {
            echo $output;
        }
    }

}

// End of file DisplayHook.php
// Location: ./hooks/DisplayHook.php
