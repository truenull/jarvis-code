<?php

/**
 * This file contains the miscellaneous functions needed in assignment display.
 * Might be refactored later to break them out in more descriptive helpers.
 *
 * PHP version 5
 *
 * @category Helpers
 * @package  jarvis
 * @author   Daniel Lundqvist <luda09yl@student.hj.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Redirects to install/migrations if needed.
 * @author   Anders Johansson <joan08c_@student.hj.se>
 * @return void
 */
function check_db_version()
{
    $ci = &get_instance();

    // Redirect to install if we're on no version.
    $ci->load->library('migration');
    if (!$ci->db->table_exists('migrations'))
    {
        redirect('install');
    }
    $version = $ci->db->get('migrations');
    if ($version->num_rows() == 0)
    {
        redirect('install');
    }
    if($version->row()->version == 0)
    {
        redirect('install');
    }
    
    $current = $version->row();
    // Redirect to migrate if we are not on current version
    $ci->config->load('migration');
    if ($current->version != $ci->config->item('migration_version'))
    {
        redirect('migrera');
    }
}

/**
 * Checks if there is a variable named the contents of name.
 * @param string $name
 * @param string $name
 * @return string Content of the variable named 'name' or contents of default
 */
function set_or_default($name, $default)
{
    if (isset($$name))
    {
        return $$name;
    }
    else
    {
        return $default;
    }
}

/**
 * 
 */
function status_icon($assignment_status)
{
    $base = ' glyphicon-';
    switch ($assignment_status)
    {
        case assignment_status::not_turned_in:
            return $base . 'log-in';
            break;
        case assignment_status::under_grading:
            return $base . 'cog';
            break;
        case assignment_status::failed:
            return $base . 'ban-circle';
            break;
        case assignment_status::approved:
            return $base . 'check';
            break;
    };
}

/**
 *
 */
function status_css($assignment_status)
{
    $base = ' btn-';
    switch ($assignment_status)
    {
        case assignment_status::not_turned_in:
            return $base . 'danger';
            break;
        case assignment_status::under_grading:
            return $base . 'warning';
            break;
        case assignment_status::failed:
            return $base . 'disabled';
            break;
        case assignment_status::approved:
            return $base . 'success';
            break;
    }
}
function get_btn_size($size)
{
    $size_class = '';
    switch ($size)
    {
        case 0:
            $size_class = ' btn-xs';
            break;
        case 1:
            $size_class = ' btn-sm';
            break;
        case 2:
            $size_class = ' btn-md';
            break;
        case 3:
            $size_class = ' btn-lg';
            break;
    }
    return $size_class;
}
/**
 * Outputs a link to the edit/add assignment function
 * @param type $course_id
 * @param type $assignment_id
 * @param type $size
 * @return string
 */
function uppgift_nav($course_id, $assignment_id = 0, $size = 2)
{
    $course_id     = intval($course_id);
    $assignment_id = intval($assignment_id);
    $size_class    = get_btn_size($size);
    
    $text    = ($assignment_id === 0 ) ? '<span class="glyphicon glyphicon-plus"> Ny uppgift</span>' : '<span class="glyphicon glyphicon-pencil"></span>';
    $action  = ($assignment_id === 0 ) ? 'ny' : 'andra';
    $url_end = (($assignment_id === 0) ? '' : ('/' . $assignment_id));
    $url     = site_url('uppgift/' . $action . '/' . $course_id . $url_end);
    $return = '<a href="' . $url
            . '" class="btn btn-default' . $size_class . '"> '
            . $text . '</a>';
    return $return;
}

/**
 * Outputs a link to the edit/add assignment function
 * @param type $course_id
 * @param type $assignment_id
 * @param type $size
 * @return string
 */
function uppgift_remove($assignment_id, $size = 2)
{
    $assignment_id = intval($assignment_id);
    $size_class    = get_btn_size($size);
    
    $text    = '<span class="glyphicon glyphicon-remove"> </span>';
    $url     = site_url('uppgift/ta_bort/' . $assignment_id );
    $return = '<a href="' . $url
            . '" class="btn btn-danger' . $size_class . '"> '
            . $text . '</a>';
    return $return;
}

/**
 * Outputs danger if mandatory and default if not.
 * @param boolean $mandatory
 */
function mandatory_button($mandatory)
{
    return ($mandatory == TRUE) ? 'primary' : 'default';
}

