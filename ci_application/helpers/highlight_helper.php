<?php

/**
 * This file contains helpers for using the syntax highlighting library Luminous
 *
 * PHP version 5
 *
 * @category Helpers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'third_party/luminous/luminous.php';
luminous::set('cache', FALSE);
luminous::set('include-javascript', TRUE);
luminous::set('include-jquery', FALSE);
luminous::set('relative-root', base_url('static/vendor/luminous'));
luminous::set('theme', 'luminous_light');
luminous::set('auto-link', TRUE);

/**
 * Checks if there is a variable named the contents of name.
 * @param string $code
 * @param string $type
 * @return string Highlighted code
 */
function highlight($code, $type = FALSE)
{
    if ($type === FALSE)
    {
        $type = luminous::guess_language($code);
    }

    return luminous::highlight($type, $code);
}

function luminous_head()
{
    return luminous::head_html();
}

