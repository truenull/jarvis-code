<?php

/**
 * This file contains the bootstrap forms helper.
 *
 * PHP version 5
 *
 * @category Helpers
 * @package  jarvis
 * @author   Daniel Lundqvist <luda09yl@student.hj.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Checks the validation states and returns the proper bootstrap class.
 * @param string $name
 * @return string Validation state
 */
function ValidationState($name)
{
    $message = form_error("$name");
    if (strlen($message) > 0)
        return "has-error";
    else
        return "has-success";
}
?>
