SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `admin_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE IF NOT EXISTS `assignments` (
  `assignment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `assignment_number` tinyint(3) unsigned NOT NULL,
  `assignment_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `assignment_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `course_id` smallint(5) unsigned NOT NULL,
  `due_date` int(10) unsigned DEFAULT NULL,
  `max_points` tinyint(3) unsigned NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  PRIMARY KEY (`assignment_id`),
  KEY `course_id` (`course_id`),
  KEY `course_id_2` (`course_id`),
  KEY `course_id_3` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

DROP TABLE IF EXISTS `assignment_comment`;
CREATE TABLE IF NOT EXISTS `assignment_comment` (
  `comment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) unsigned NOT NULL,
  `user_id` smallint(5) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `commenter_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `course_id` (`assignment_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `commenter_id` (`commenter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

DROP TABLE IF EXISTS `assignment_files`;
CREATE TABLE IF NOT EXISTS `assignment_files` (
  `assignment_file_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) unsigned NOT NULL,
  `file_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`assignment_file_id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `course_tag` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `course_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `course_description` text COLLATE utf8_unicode_ci,
  `end_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `registrations`;
CREATE TABLE IF NOT EXISTS `registrations` (
  `user_id` smallint(5) unsigned NOT NULL,
  `course_id` smallint(5) unsigned NOT NULL,
  `access_level` tinyint(3) unsigned NOT NULL COMMENT '10 = Student, 20 = Read Only Teacher, 30 = Full Teacher',
  PRIMARY KEY (`user_id`,`course_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `reset_tokens`;
CREATE TABLE IF NOT EXISTS `reset_tokens` (
  `user_id` smallint(5) unsigned NOT NULL,
  `reset_token` char(30) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `reset_token_FI` (`reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `student_assignments`;
CREATE TABLE IF NOT EXISTS `student_assignments` (
  `user_id` smallint(5) unsigned NOT NULL,
  `assignment_id` bigint(20) unsigned NOT NULL,
  `grade` smallint(6) NOT NULL COMMENT 'How many points that have been acquired for this task.',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Not turned in = 0, Changes needed = 10, Failed = 20, Approved =  30',
  PRIMARY KEY (`user_id`,`assignment_id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `student_files`;
CREATE TABLE IF NOT EXISTS `student_files` (
  `user_id` smallint(5) unsigned NOT NULL,
  `assignment_file_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `upload_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`assignment_file_id`),
  KEY `assignment_file_id` (`assignment_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(123) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Length = maximum value of crypt php function',
  `last_login` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=110 ;


ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `assignment_comment`
  ADD CONSTRAINT `assignment_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignment_comment_ibfk_2` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`assignment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignment_comment_ibfk_3` FOREIGN KEY (`commenter_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `assignment_files`
  ADD CONSTRAINT `assignment_files_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`assignment_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `registrations`
  ADD CONSTRAINT `registrations_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `registrations_ibfk_3` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `reset_tokens`
  ADD CONSTRAINT `reset_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `student_assignments`
  ADD CONSTRAINT `student_assignments_ibfk_2` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`assignment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `student_assignments_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `student_files`
  ADD CONSTRAINT `student_files_ibfk_1` FOREIGN KEY (`assignment_file_id`) REFERENCES `assignment_files` (`assignment_file_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `student_files_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

REPLACE INTO `users` (`user_id`, `user_name`, `email`, `password_hash`, `last_login`) VALUES
(1, 'System', 'no-one@nowhere.invalid', 'invalid', 0);

SET FOREIGN_KEY_CHECKS=1;
