<?php
/**
 * This file contains the controller for the add/edit course pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The course admin pages of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Kurs extends My_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
    }

    function ta_bort($course_id)
    {
        $course = $this->_check_course($course_id);
        if (!$this->permission_model->is_teacher(
                        $this->user->user_id, $course_id))
        {
            show_error('Du har inte befogenhet att lägga'
                    . 'till uppgifter till denna kurs.', 403);
        }

        $this->load->view('header',
                          array(
            'header'     => 'Ändra kurs',
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
        ));


        // Ask for confirmation
        $this->load->view('kurser/remove_confirmation',
                          array('course' => $course));
        $this->load->view('footer');

    }

    public function saker($course_id)
    {
        $course = $this->_check_course($course_id);
        if (!$this->permission_model->is_teacher(
                        $this->user->user_id, $course_id))
        {
            show_error('Du har inte befogenhet att lägga'
                    . 'till uppgifter till denna kurs.', 403);
        }

        $this->course_model->delete_course($course_id);

        // redir to course list to show results
        redirect('kurser/lista/OK');
    }

    /**
     * Form for changing a course.
     * 
     * @param int $course_id
     */
    function andra($course_id = 0)
    {
        if ($course_id == 0)
        {
            show_error(
                    'Om du fick detta meddelandet så har vi en URL bug.'
                    . 'Vänligen rapportera var länken som du klickade på finns.'
            );
        }
        $course = $this->course_model->get_course($course_id);
        if ($course === false)
        {
            show_error('Kursen kunde ej hittas.');
        }
        if (!$this->permission_model->is_teacher(
                        $this->user->user_id, $course_id))
        {
            show_error('Du har inte befogenhet att lägga'
                    . 'till uppgifter till denna kurs.', 403);
        }

        $this->load->model('assignment_model');
        $data = array(
            'course'   => $course,
            'tag'      => $course->course_tag,
            'name'     => $course->course_name,
            'desc'     => $course->course_description,
            'end_time' => date('Y-m-d', $course->end_time),
            'assignments' => $this->assignment_model->get_assignments($course_id)
        );

        $this->load->view('header',
                          array(
            'header'     => 'Ändra kurs',
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
        ));

        $this->load->view('kurser/edit_course', $data);
        $this->load->view('footer');
    }

    function mottag()
    {
        $this->_set_uppgift_formvalidation();

        // Fetch course
        $course = $this->_check_course($_POST['course_id']);


        // run form validation so set_value() gets populated
        $validated = $this->form_validation->run();
        // fill data array
        $data = array(
            'course'   => $course,
            'tag'      => set_value('tag'),
            'name'     => set_value('name'),
            'desc'     => set_value('desc'),
            'end_time' => set_value('end_time')
        );

        if ($validated === FALSE)
        {
            // Redisplay form
            $this->load->view('header',
                              array(
                'header'     => 'Ändra kurs',
                'course'     => $course,
                'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
            ));
            $this->load->view('kurser/edit_course', $data);
            $this->load->view('footer');
        }
        else
        {
            $data['end_time'] = $this->_get_time($data['end_time']);

            $this->_update_course($data, $course->course_id);

            redirect('uppgifter/lista/OK');
        }
    }

    /**
     * Shows an error if the course does not exist.
     * @param int $course_id
     * @return course_record
     */
    function _check_course($course_id)
    {
        $course_id = intval($course_id);
        $course    = $this->course_model->get_course($course_id);
        if ($course === FALSE)
        {
            show_error('Kursen kunde ej hittas.' . intval($_POST['course_id']));
        }
        return $course;
    }

    /**
     * Callback function that checks if a YYYY-MM-DD string is a valid date.
     * @author Anders Johansson <anders@truenull.se>
     *
     * @param string $date
     * @return boolean
     */
    function _check_date($date)
    {
        // Expecting YYYY-MM-DD
        $ymd = explode('-', $date, 3);
        if (count($ymd) === 3)
        {
            return checkdate(intval($ymd[1]), intval($ymd[2]),
                                                     intval($ymd[0]) === TRUE);
        }
        return FALSE;
    }

    /**
     * Converts time to unix time.
     * @param type $date
     * @param type $hour
     * @param type $minute
     * @param type $second
     * @return int
     */
    function _get_time($date, $hour = 22, $minute = 59, $second = 59)
    {
        $ymd   = explode('-', $date, 3);
        $year  = intval($ymd[0]);
        $month = intval($ymd[1]);
        $day   = intval($ymd[2]);
        
        return mktime($hour, $minute, $second, $month, $day, $year);
    }

    function _set_uppgift_formvalidation()
    {
        $this->load->library('form_validation');

        // Validation rules
        $this->form_validation->set_rules('tag', 'tag',
                                          'trim|required|max_length[10]|prep_for_form');
        $this->form_validation->set_rules('name', 'namn',
                                          'trim|required|max_length[40]|prep_for_form');
        $this->form_validation->set_rules('desc', 'beskrivning',
                                          'trim|max_length[8000]|prep_for_form');
        $this->form_validation->set_rules('course_id', 'kurs id',
                                          'required|integer');
        $this->form_validation->set_rules('end_time', 'sista inlämningsdag',
                                          'required|prep_for_form|callback__checkdate');

        // Custom error message for date
        $this->form_validation->set_message('end_time',
                                            'Du måste fylla i ett '
                . 'giltigt datum på formen YYYY-MM-DD'
        );
        $this->form_validation->set_error_delimiters('<p class="form-control">',
                                                     '');
    }

    function _update_course($data, $course_id)
    {
        $this->course_model->set_desc($data['desc'], $course_id);
        $this->course_model->set_name($data['name'], $course_id);
        $this->course_model->set_tag($data['tag'], $course_id);
        $this->course_model->set_end_time($data['end_time'], $course_id);
    }
}

// End of file kurs.php
// Location: ./controllers/kurs.php