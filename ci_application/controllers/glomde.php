<?php
/**
 * This file contains the controller for the forgot password feature.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The forgot password pages of the application.
 *
 * @property user_model $user_model
 * @property CI_Email $email Description
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Glomde extends CI_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        check_db_version();
        
        $this->load->library('email');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->formular();
    }

    /**
     * Provides a form with an email field. Regardless if the email was found or
     * not it provides a message about checking your email.
     */
    public function formular()
    {
        $this->_wait();

        $this->load->view('header',
                          array(
            'user_name' => '',
            'header'    => 'Begär nytt lösenord',
            'hideNav'   => TRUE,
            'right'     => 0,
            'left'      => 12
        ));

        if (!isset($_POST['jarvisemail']))
        {
            $this->load->view('glomde/email');
        }
        else
        {
            $user_id = $this->user_model->get_user_id($_POST['jarvisemail']);
            if ($user_id !== FALSE)
            {
                $user = $this->user_model->get_user($user_id);
                $created = FALSE;
                if($user != FALSE)
                {
                    $created = $this->user_model->create_reset_token_by_id($user->user_id);
                }
                if ($created)
                {
                    $reset_token = $this->user_model->get_reset_token($user->user_id);
                    if ($reset_token)
                    {
                        // successfully got reset token, time to email it to the user!
                        $this->_send_reset_mail($user, $reset_token);
                    }
                }
            }
            // load message regardless of status
            $this->load->view('glomde/email_message');
        }
        $this->load->view('footer');
    }

    /**
     * If the token matches a password form is shown.
     * If not an error about invalid token is shown.
     * @param type $password_reset_token
     */
    public function token($reset_token = FALSE)
    {
        $this->_wait();


        $this->load->view('header',
                          array(
            'user_name' => '',
            'header'    => 'Lösenordsåterställning',
            'hideNav'   => TRUE,
            'right'     => 0,
            'left'      => 12
        ));
        $this->load->view('glomde/password',
                          array('reset_token' => $reset_token));
        $this->load->view('footer');
    }

    public function andra()
    {
        $this->_wait();

        $this->form_validation->set_error_delimiters('<p class="help-block">',
                                                     '</p>');
        // Form validation rules
        $this->form_validation->set_rules(
                'reset_token', 'återställnings koden',
                'trim|required|callback__check_token'
        );
        $this->form_validation->set_rules(
                'jarvispass', 'lösenord',
                'trim|required|min_length['
                . user_model::min_pass_length . ']|max_length['
                . user_model::max_pass_length
                . ']|max_length[2048]|matches[jarvispassconf]');
        $this->form_validation->set_rules(
                'jarvispassconf', 'lösenordsbekräftelse', 'trim|required');

        $this->load->view('header',
                          array(
            'user_name' => '',
            'header'    => 'Lösenordsåterställning',
            'hideNav'   => TRUE,
            'right'     => 0,
            'left'      => 12
        ));
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('glomde/token');
        }
        else
        {
            $t = $this->user_model->get_reset_token_by_token(set_value('reset_token'));
            if ($t === FALSE)
            {
                // We checked before so it should still be here.
                show_error('Kunde ej hitta lösenordsändringskoden vid ändringstillfället.');
            }
            // do actual reset
            $sucess = $this->user_model->set_password(set_value('jarvispass'),
                                                                $t->user_id);
            if ($sucess == FALSE)
            {
                show_error('Kunde ej byta lösenord.');
            }
            // remove reset token
            $this->user_model->remove_reset_token($t->reset_token);

            $this->load->view('glomde/password_success');
        }
        $this->load->view('footer');
    }

    public function _send_reset_mail($user, $reset_token)
    {
        $this->load->config('email');
        $this->email->from(
                $this->config->item('system_from'),
                                    $this->config->item('system_from_name')
        );
        $this->email->bcc($user->email);

        $this->email->subject('Lösenordsåterställningsbegäran');

        $ip         = $_SERVER['REMOTE_ADDR'];
        $reset_link = site_url('glomde/token/' . $reset_token->reset_token);
        $reset_form = site_url('glomde/token');
        $date       = date('Y-m-d H:i:s');
        $message    = <<<EOD
<h1>Hejsan</h1>
<p>En användare på IP nummer "$ip" har vid tiden $date begärt återställning av ert lösenord.
Om det inte var du som begärde detta så behöver du inte göra något.
Begäran kommer automatiskt att försvinna om ett antal timmar.
<p>Om det var du som gjorde begäran var god besök
{unwrap}<a href="$reset_link">denna länk.</a>{/unwrap}
<p>Om länken inte fungerar försök att istället gå till
<p>
{unwrap}$reset_form{/unwrap}
<p>och mata in nästa rad i token fältet.
<p>
{unwrap}$reset_token->reset_token{/unwrap}
<p>Mvh, (i alla fall så vänliga som mina kretsar tillåter...)<br/>
    e-lärsystemet Jarvis
EOD;
        $this->email->message($message);
        $this->email->send();
    }

    public function _check_token($token)
    {
        $token = $this->user_model->get_reset_token_by_token($token);

        if ($token === FALSE)
        {
            $this->form_validation->set_message('reset_token',
                                                'Den %s du angav kunde ej verifieras. Kanske är den redan använd?');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

        /**
     * Randomly wait up to a second so email slurpers can't use time to
     * determine if the user exists or not.
     */
    public function _wait()
    {
        $time = mt_rand(200000, 999999);
        usleep($time);
    }

}

// End of file glomde.php
// Location: ./controllers/glomde.php