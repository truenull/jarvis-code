<?php
/**
 * This file contains the controller for the course list pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The course list page of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Kurser extends My_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     *  Shows the list of courses
     * 
     * @return view List of courses
     */
    public function index()
    {
        $this->lista();
    }

    /**
     * Show the list of courses that the user has a registration for. (with edit course modal button?)
     *
     * @todo Implement
     * @return view List of courses
     */
    public function lista($op = 0)
    {
        $message = $this->_get_op_text($op);
        $data = array(
            'courses' => $this->course_model->get_all_courses($this->user->user_id),
            'message' => $message,
            'user'    => $this->user,
            'is_admin' => $this->permission_model->is_admin($this->user->user_id)
        );
        $course = $this->_get_course_or_default(0);

        $this->load->view('header', array(
            'header' => 'Kurslista',
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
            'isTeacher' => $this->permission_model->is_admin(
                        $this->user->user_id) || $this->permission_model->is_teacher(
                        $this->user->user_id,
                        $course->course_id
                    )
        ));

        $this->load->view('kurser/lista.php', $data);
		
        $this->load->view('footer');
    }

    private function _get_op_text($op)
    {
        if ($op === 0)
            return '';

        $message = '';
        switch ($op)
        {
            case 'OK':
                $message = 'Åtgärden lyckades!';
                break;
        }
        return <<<EOT
<div class="row has-success">
<p class="help-block">$message
</div>
EOT;
    }	
}

// End of file kurser.php
// Location: ./controllers/kurser.php