<?php
/**
 * This file contains the controller for the assignment pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The assignment pages of the application.
 *
 * @property turnin_model $turnin_model
 * 
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Uppgifter extends My_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        $this->load->model('turnin_model');
        $this->load->helper('misc_helper');
        $this->load->helper('highlight_helper');
    }

    /**
     *  Shows the teacher assignments matrix or the user
     *
     * @todo Implement
     * @return view assignment page
     */
    public function index()
    {
        $this->auto();
    }

    /**
     * Since we can't have parameters on index function, we use auto (select)
     * @param int $course_id
     */
    public function auto($course_id = 0, $op_status = 0)
    {
        // get the users default course
        $course = $this->_get_course_or_default($course_id, $this->user->user_id);

        if ($course === FALSE)
        {
            show_error('Du verkar inte vara registerad på någon kurs. '
                    . 'Om du anser att detta är felaktigt var god kontakta kurslärare.');
        }
        else if ($this->permission_model->is_student($this->user->user_id,
                                                     $course->course_id))
        {
            $this->elev($this->user->user_id, $course->course_id);
        }
        else
        {
            $this->matris($course->course_id);
        }
    }

    /**
     * Show the Student assignment page
     *
     * @todo Implement
     * @return view Student assignment page
     */
    public function elev($user_id, $course_id = 0)
    {
        $course = $this->_get_course_or_default($course_id, $user_id);

        $joyride_stops = array();
        $this->_get_modal_button_help($joyride_stops);
        
        $this->load->view('header', array(
            'header'     => 'Elevuppgifter',
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
            'active'        => 1,
            'extra_headers' => array(
                luminous_head(),
                "<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>"
            ),
            'override_nav_target' => "uppgifter/elev/$user_id/",
            'max_points'          => $this->assignment_model->get_point_sum($course->course_id),
            'joyride_stops'       => $joyride_stops,
        ));

//        $data = array();
//        $data['turnins']   = $this->turnin_model->get_all_student_turnins($course_id,
//                                                                          $user_id);
//        $data['user']      = $this->user_model->get_user($user_id);
//        $data['userid']    = $this->user->user_id;
//        $data['courseid']  = $course_id;
//        $data['isteacher'] = $this->permission_model->is_teacher($this->user->user_id,
//                                                                 $course_id)
//                            || $this->permission_model->is_limited_teacher(
//                                    $this->user->user_id,
//                                    $course_id);
//        $data = $accumulated_points = $this->deltagare_model->get_accumulated_points($course->course_id);

        $this->load->view('uppgifter/student', array(
            'turnins'               => $this->turnin_model->get_all_student_turnins($course_id,$user_id),
            'user'                  => $this->user_model->get_user($user_id),
            'userid'                => $this->user->user_id,
            'courseid'              => $course_id,
            'isteacher'             => $this->permission_model->is_teacher($this->user->user_id,
                                                    $course_id)
                                        || $this->permission_model->is_limited_teacher(
                                                    $this->user->user_id, $course_id),
            'accumulated_points'    => $this->deltagare_model->get_accumulated_points($course->course_id, $user_id)
            ));
                //$data);

        $this->load->view('footer');
    }

    /**
     * Show the Teacher assignment matrix page
     *
     *
     * @return view Teacher assignment matrix page
     * */
    public function matris($course_id = 0, $op_status = 0)
    {
        $op_text = $this->_get_op_text($op_status);
        
        // Check input and permissions
        if ($course_id != 0)
        {
            $course = $this->course_model->get_course($course_id);
        }
        else
        {
            $course = $this->user_model->get_default_course($this->user->user_id);
        }

        if (!$course)
        {
            show_error('Kursen hittades ej.');
        }
        $is_teacher = $this->permission_model->is_teacher(
                $this->user->user_id, $course->course_id
        );
        if (!$is_teacher)
        {
            show_error('Du har inte tillåtelse att titta på lärarvyn.');
        }

        // Get stuff
        $turnins = $this->turnin_model->get_all_student_turnins($course->course_id);

        $navCourses = $this->user_model->get_latest_courses($this->user->user_id);
        $turnin_stops = array('userLinkStop' => '<h1>Användarvyn</h1>'
            .'<p>Du kan klicka här för att gå till användarvyn.'
            .'<p>Om det är flera som har samma namn kan du särskilja dem på mejlen i användarvyn och i rättningsfönstret.');
        $this->_get_modal_button_help($turnin_stops);
        
        $this->load->helper('highlight_helper');
        $this->load->view('header',
                          array(
            'course'     => $course,
            'navCourses' => $navCourses,
            'header'     => 'Lärarmatris',
            'fill'       => TRUE,
            'active'        => 1,
            'joyride_stops' => $turnin_stops,
            'extra_headers' => array(
                luminous_head(),
                "<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>")
        ));

         $this->load->view('uppgifter/matris',
          array(
          'turnins' => &$turnins,
          'course'  => $course,
          'op_text' => $op_text,
          )); 

        $this->load->view('footer');
    }

    public function teacher_modal($user_id, $assignment_id)
    {
        $data['assignment']          = $this->assignment_model->get_assignment($assignment_id);
        $data['user']                = $this->user_model->get_user($user_id);
        $data['assignment_files']    = $this->assignment_model->get_assignment_files($assignment_id);
        $data['assignment_status']   = $this->turnin_model->get_assignment_status($user_id,
                                                                                  $assignment_id);
        $data['assignment_comments'] = $this->turnin_model->get_assignment_comments($user_id,
                                                                                    $assignment_id);

        $this->load->view('uppgifter/teacher_modal', $data);
    }

    public function teacher_nonmodal($user_id, $assignment_id)
    {
        $assignment = $this->assignment_model->get_assignment($assignment_id);
        if (!$assignment)
        {
            show_error('Uppgiften kunde ej hittas.');
        }

        $this->load->view('header',
                          array(
            'user_name'  => '',
            'header'     => 'Inlämningsfönster',
            'hideNav'    => FALSE,
            'course'     => $this->course_model->get_course($assignment->course_id),
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
            'extra_headers' => array(
                luminous_head(),
                "<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>"
            ),
            'nonmodal'   => TRUE
        ));

        $this->teacher_modal($user_id, $assignment_id);

        $this->load->view('footer');
    }

    /**
     * Inserts grading/comments into database.
     * When this function is called we assume the data has already been validated.
     *
     * @return view Teacher assignment matrix page
     * */
    public function teacher_submit($user_id, $assignment_id, $course_id = 0)
    {
        // Make sure the user has permission to grade.
        $has_access = false;
        $course     = $this->assignment_model->get_assignment($assignment_id)->course_id;
        if ($this->permission_model->is_limited_teacher($this->user->user_id,
                $course) || $this->permission_model->is_teacher($this->user->user_id,
                $course) || $this->permission_model->is_admin($this->user->user_id))
        {
            $has_access = true;
        }
        if (!$has_access)
        {
            show_error('Du har inte tillgång till denna funktion.');
        }

        // Create comment.
        $_POST['comment'] = trim($_POST['comment']);
        if ($_POST['comment'] != "")
            $this->turnin_model->create_comment($user_id, $assignment_id,
                                                $this->session->userdata('user_id'),
                                                $_POST['comment']);
        // Set grade.
        $changed_grade = 0;
        if ($_POST['grade'] != "")
            $changed_grade = $this->turnin_model->set_grade($user_id,
                                                        $assignment_id,
                                                        $_POST['grade']);
        // Set status.
        $changed_status = 0;
        if ($_POST['betyg'] != "")
            $changed_status = $this->turnin_model->set_status($user_id,
                                                          $assignment_id,
                                                          $_POST['betyg']);

        // Send system message about changes to grade and/or status.
        if ($changed_grade === 1 && $changed_status === 1)
        {
            $this->turnin_model->create_comment($user_id, $assignment_id, 1,
                                                "Poäng ändrade till " . $_POST['grade'] . " och status ändrad.");
        }
        else if ($changed_grade === 1)
        {
            $this->turnin_model->create_comment($user_id, $assignment_id, 1,
                                                "Poäng ändrade till " . $_POST['grade'] . ".");
        }
        else if ($changed_status === 1)
        {
            $this->turnin_model->create_comment($user_id, $assignment_id, 1,
                                                "Status ändrad.");
        }

        redirect('uppgifter/auto/' . $course_id, 'location');
    }

    public function student_modal($user_id, $assignment_id)
    {
        $data['assignment']          = $this->assignment_model->get_assignment($assignment_id);
        $data['user']                = $this->user_model->get_user($user_id);
        $data['assignment_files']    = $this->assignment_model->get_assignment_files($assignment_id);
        $data['assignment_status']   = $this->turnin_model->get_assignment_status($user_id,
                                                                                  $assignment_id);
        $data['assignment_comments'] = $this->turnin_model->get_assignment_comments($user_id,
                                                                                    $assignment_id);
        $data['himself'] = $user_id == $this->user->user_id;
        $this->load->view('uppgifter/student_modal', $data);
    }

    public function student_nonmodal($user_id, $assignment_id)
    {
        $assignment = $this->assignment_model->get_assignment($assignment_id);
        if (!$assignment)
        {
            show_error('Uppgiften kunde ej hittas.');
        }

        $this->load->view('header',
                          array(
            'user_name'  => '',
            'header'     => 'Inlämningsfönster',
            'hideNav'    => FALSE,
            'course'     => $this->course_model->get_course($assignment->course_id),
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
            'extra_headers' => array(
                luminous_head(),
                "<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>"
            ),
            'nonmodal'   => TRUE
        ));

        $this->student_modal($user_id, $assignment_id);

        $this->load->view('footer');
    }

    /**
     * Inserts grading/comments into database.
     * When this function is called we assume the data has already been validated.
     *
     * @return view Teacher assignment matrix page
     * */
    public function student_submit($user_id, $assignment_id, $course_id = 0)
    {
        if($user_id != $this->user->user_id)
            show_error ('En användare kan inte lämna in åt andra användare.');

        // Create comment.
        $_POST['comment'] = trim($_POST['comment']);
        if ($_POST['comment'] != "")
            $this->turnin_model->create_comment($user_id, $assignment_id,
                                                $this->session->userdata('user_id'),
                                                                         $_POST['comment']);

        // Upload files.
        $assignment_status = $this->turnin_model->get_assignment_status($user_id, $assignment_id);
        if (!isset($assignment_status->status) || $assignment_status->status == 0 || $assignment_status->status == 10)
        {
            $assignment_files = $this->turnin_model->get_all_assignment_files($assignment_id);

            $this->load->model('upload_model');
            foreach ($assignment_files as $file)
            {
                $key = 'file' . $file->assignment_file_id;
                if(array_key_exists($key, $_FILES) && $_FILES[$key]['tmp_name'] != '')
                {
                    print_r($file);
                    $this->upload_model->save_file($this->user->user_id, $file->assignment_file_id);
                }
            }
            $this->turnin_model->eval_assignment_status($user_id, $assignment_id);
        }
        
        redirect('uppgifter/elev/' . $user_id . "/" . $course_id, 'location');
    }

    private function _get_op_text($op_status)
    {
        if (is_numeric($op_status))
            return '';

        $message = '';
        switch (trim($op_status))
        {
            case 'OK':
                $message = 'Åtgärden utfördes korrekt!';
                break;
        }
        return <<<EOT
<div class="has-success">
<p class="help-block">$message
</div>
EOT;
    }

    /**
     * Adds a modalButtonStop index entry to $data with help html for modal buttons.
     * @param array(string) $data
     */
    private function _get_modal_button_help(&$data)
    {
        $data['modalButtonStop'] = '<h1>Statusknappar</h1>'
                . '<p>Om du klickar på dem öppnas ett fönster för den elevens inlämningar av den uppgiften.'
                . '<p>Om du vill så kan du öppna dem i ett nytt fönster eller en '
                . 'ny flik som du vanligen gör i din webbläsare (mittenmusknappen eller genom högermusklicksmenyn).<div class="btn-group-vertical">';
        
        $status = assignment_status::not_turned_in;
        $data['modalButtonStop'] = $data['modalButtonStop'] . '<a href="#" class="btn btn'.  status_css($status)
                .'"><span class="glyphicon glyphicon'.status_icon($status).'"> Ej Inlämnad&nbsp;</span></a>';

        $status = assignment_status::under_grading;
        $data['modalButtonStop'] = $data['modalButtonStop'] . '<a href="#" class="btn btn'.  status_css($status)
                .'"><span class="glyphicon glyphicon'.status_icon($status).'"> Fullt Inlämnad</span></a>';

        $status = assignment_status::failed;
        $data['modalButtonStop'] = $data['modalButtonStop'] . '<a href="#" class="btn btn'.  status_css($status)
                .'"><span class="glyphicon glyphicon'.status_icon($status).'"> Underkänd uppgift</span></a>';
        
        $status = assignment_status::approved;
        $data['modalButtonStop'] = $data['modalButtonStop'] . '<a href="#" class="btn btn'.  status_css($status)
                .'"><span class="glyphicon glyphicon'.status_icon($status).'"> Godkänd uppgift</span></a></div><p>';


    }
}

// End of file uppgifter.php
// Location: ./controllers/uppgifter.php
