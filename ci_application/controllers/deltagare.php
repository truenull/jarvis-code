<?php
/**
 * This file contains the controller for the participant administration page of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Jonathan Ekelund <jonathan.ekelund1@gmail.com>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class deltagare extends My_Controller
{

    /**
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('deltagare_model');
        $this->load->library('form_validation');
    }
	
	public function index()
	{
		$this->auto();
	}
	
	public function auto($course_id = 0)
	{
        $isteacher = $this->permission_model->is_teacher($this->user->user_id,
                                                         $course_id);

        //get the users default course
		$course = $this->_get_course_or_default($course_id, $this->user->user_id);
		
		$navCourses = $this->user_model->get_latest_courses($this->user->user_id);

		 $this->load->view('header',
                          array(
            'course'    => $course,
            'navCourses' => $navCourses,
            'header'     => 'Deltagare',
            'active'     => 2
        ));
		
	$all_members = $this->deltagare_model->get_members($course->course_id);
        $accumulated_points = $this->deltagare_model->get_accumulated_points($course->course_id);
        $is_student = $this->permission_model->is_student($this->session->userdata['user_id'], $course_id);
        $this->load->view('deltagare/deltagare-view',
            array(
                    'isteacher'             => $isteacher,
                    'course'                => $course,
                    'all_members'           => $all_members,
                    'accumulated_points'    => $accumulated_points,
                    'is_student'            => $is_student,
                    'max_points'            =>$this->assignment_model->get_point_sum($course->course_id),
                )
        );
        $this->load->view('footer');
	}

    public function ta_bort($course_id, $user_id)
    {
        $has_access = $this->permission_model->is_teacher($this->user->user_id, $course_id)
                || $this->permission_model->is_limited_teacher($this->user->user_id, $course_id);
        if($has_access === FALSE)
        {
            show_error('Du har inte tillåtelse att ta bort elever från denna kurs.');
        }

        $this->course_model->unregister($user_id, $course_id);
        redirect('deltagare');
    }

    public function lagg_till($course_id)
    {
        //get the users default course
        $course = $this->_get_course_or_default($course_id, $this->user->user_id);

        $navCourses = $this->user_model->get_latest_courses($this->user->user_id);

        $this->load->view('header',
                          array(
            'course'     => $course,
            'navCourses' => $navCourses,
            'header'     => 'Lägg till deltagare',
            'active'     => 2
        ));
        $this->load->view('deltagare/add_users', array('course' => $course));
        $this->load->view('footer');
    }

    public function mottag()
    {
        $course = $this->course_model->get_course($_POST['course_id']);
        if ($course == FALSE)
        {
            show_error('Den specifierade kursen finns inte.');
        }
        $navCourses = $this->user_model->get_latest_courses($this->user->user_id);
        
        $is_teacher = $this->permission_model->is_teacher(
                        $this->user->user_id, $course->course_id)
                || $this->permission_model->is_limited_teacher(
                        $this->user->user_id, $course->course_id);
        if ($is_teacher == FALSE)
        {
            show_error("Du verkar inte ha tillgång till denna funktion.");
        }


        $this->form_validation->set_rules('namef', 'namn',
                                          'trim|max_length[50]|prep_for_form');
        $this->form_validation->set_rules('emailf', 'email',
                                          'trim|valid_email|max_length[70]|prep_for_form');
        $this->form_validation->set_error_delimiters('<p class="form-control">',
                                                     '');
        $valid_form = $this->form_validation->run();
        if ($_FILES['userfile']['tmp_name'] != '')
        {
            // Add by csv
            if ($_FILES['userfile']['tmp_name'] != '')
            {
                $contents = file_get_contents($_FILES['userfile']['tmp_name']);
                $user_ids = $this->user_model->create_from_csv($contents);
                foreach ($user_ids as $user_id)
                {
                    if (FALSE == $this->permission_model->is_student($user_id,
                                                                     $course->course_id))
                    {
                        $this->course_model->register_student($course->course_id,
                                                              $user_id);
                    }
                }
            }
            redirect('deltagare/auto/'.$course->course_id);
        }
        else
        {
            // Add by single input
            if ($valid_form)
            {
                $name    = $_POST['namef'];
                $email   = $_POST['emailf'];
                $user_id = $this->user_model->get_user_id($email);
                if ($user_id == 0)
                {
                    // user does not exist
                    $user_id = $this->user_model->create_user(
                            $name, $email,random_string());
                    if ($user_id == 0)
                    {
                        show_error('Användaren kunde inte skapas av någon anledning.');
                    }
                }
                else
                {
                    if ($this->permission_model->is_student($user_id,
                                                            $course->course_id))
                    {
                        // User is already a student of the course
                        redirect('deltagare/auto/'.$course->course_id);
                    }
                }

                // Register to course
                $this->course_model->register_student($course->course_id,
                                                      $user_id);
                redirect('deltagare/auto/'.$course->course_id);
            }
            else
            {
                // Redisplay form with errors

                $this->load->view('header',
                                  array(
                    'course'     => $course,
                    'navCourses' => $navCourses,
                    'header'     => 'Lägg till deltagare',
                    'active'     => 2
                ));
                $this->load->view('deltagare/add_users');
                $this->load->view('footer');
            }
        }
    }

}


// End of file deltagare.php
// Location: ./controllers/deltagare.php