<?php
/**
 * This file contains the controller for the assignment pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The assignment pages of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Uppgift extends My_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
    }

    /**
     *  Shows the treacher assignments matrix or the user
     *
     * @todo Implement
     * @return view assignment page
     */
    public function index()
    {
        // get the users default course
        // Check what access the user has to the course
        // Call appropriate controller function to show
        // ex: $this->elev();
        // $this->teacher();
        // or error_403() ?
    }

    function ny($course_id)
    {
        $this->andra($course_id);
    }

    /**
     * Form for adding or changing an assignment to a course.
     * 
     * @param type $course_id
     */
    function andra($course_id = 0, $assignment_id = 0)
    {
        if ($course_id == 0)
        {
            show_error(
                    'Om du fick detta meddelandet så har vi en URL bug.'
                    . 'Vänligen rapportera var länken som du klickade på finns.'
            );
        }
        if (!$this->permission_model->is_teacher(
                        $this->user->user_id, $course_id))
        {
            show_error('Du har inte befogenhet att lägga'
                    . 'till uppgifter till denna kurs.', 403);
        }

        $course = $this->course_model->get_course($course_id);
        if ($course === false)
        {
            show_error('Kursen kunde ej hittas.');
        }

        // New assignment
        $data = array(
            'course'     => $course,
            'id'         => 0,
            'number'     => 1+$this->assignment_model->get_max_assignment_number($course_id),
            'name'       => "",
            'desc'       => "",
            'due_date'   => "",
            'max_points' => 0,
            'files'      => '',
            'mandatory'  => 0
        );
        $assignment = $this->assignment_model->get_assignment($assignment_id);

        if ($assignment != FALSE)
        {
            $data ['id']         = $assignment->assignment_id;
            $data ['number']     = $assignment->assignment_number;
            $data ['name']       = $assignment->assignment_name;
            $data ['desc']       = $assignment->assignment_desc;
            $data ['due_date']   = date('Y-m-d', $assignment->due_date);
            $data ['max_points'] = $assignment->max_points;
            $data ['mandatory']  = $assignment->mandatory;

            $data ['files']      = '';
            $files          = $this->assignment_model->get_assignment_files($assignment->assignment_id);
            foreach ($files as $file)
            {
                $data ['files'] = ($file->download_only?'+':'').$data ['files'] . $file->file_name . "\n";
            }
        }

        $this->load->view('header',
            array(
                'course' => $course,
                'header'    => ($assignment == FALSE) ?
                    'Ny uppgift' :
                    'Ändra uppgift '
                    . $assignment->assignment_number,
                'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
            )
        );

        $this->load->view('uppgifter/edit_assignment', $data);
        $this->load->view('footer');
    }

    function mottag()
    {
        $this->_set_uppgift_formvalidation();

        // Fetch course
        $course        = $this->_check_course($_POST['course_id']);
        
        // Get course
        $assignment_id = intval($_POST['assignment_id']);
        $assignment    = $this->assignment_model->get_assignment($assignment_id);

        // fill data array
        $data = array(
            'course'     => $course,
            'id'         => $assignment_id,
            'number'     => intval($_POST['number']),
            'name'       => $_POST['name'],
            'desc'       => $_POST['desc'],
            'due_date'   => $_POST['due_date'],
            'max_points' => intval($_POST['max_points']),
            'files'      => $_POST['files'],
            'mandatory'  => ($_POST['mandatory'] ? 1 : 0)
        );
        //die(print_r($data['mandatory']));
        if ($this->form_validation->run() === FALSE)
        {
            // Redisplay form
            $this->load->view('header',
                              array(
                'user_name' => '',
                'header'    => ($assignment == FALSE) ?
                        'Ny uppgift' :
                        'Ändra uppgift '
                        . $assignment->assignment_number,
                'course'     => $course,
                'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
                'hideNav'   => FALSE,
                'right'     => 0,
                'left'      => 12
            ));
            $this->load->view('uppgifter/edit_assignment', $data);
            $this->load->view('footer');
        }
        else
        {
            $due_date = $this->_get_time($data['due_date']);
            if ($assignment === FALSE)
            {
                $assignment_id = $this->assignment_model->create_assignment(
                        $course->course_id, $data['number'], $data['name'],
                        $data['desc'], $due_date, $data['max_points'],
                        $data['mandatory']
                );
                $assigment     = $this->assignment_model->get_assignment($assignment_id);

                if (!$assigment)
                {
                    show_error('Assignment could not be created for some reason.');
                }

                $this->_update_files($data['files'], $assigment->assignment_id);
            }
            else
            {
                $this->_update_assignment($data, $assignment->assignment_id);
            }
            redirect('uppgifter/matris/' . $course->course_id . '/'. 'OK');
        }
    }

    /**
     * Removes the current assignment
     */
    function ta_bort($assignment_id)
    {
        $ass = $this->assignment_model->get_assignment($assignment_id);
        if($ass === FALSE)
        {
            show_error('Uppgiften finns ej.');
        }
        $course = $this->course_model->get_course($ass->course_id);

        $this->load->view('header', array(
                'course' => $course,
                'header'    => 'Ta bort uppgift ' . $ass->assignment_number,
                'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
            ));
        $this->load->view('uppgifter/remove_confirmation', array(
                    'course'     => $course,
                    'assignment'  => $ass
                ));
        $this->load->view('footer');
    }

    public function saker($assignment_id)
    {
        $ass = $this->assignment_model->get_assignment($assignment_id);
        if($ass === FALSE)
        {
            show_error('Uppgiften finns ej.');
        }
        $course = $this->course_model->get_course($ass->course_id);

        if($_POST['confirmation'] == 'yes')
        {
            $files = $this->assignment_model->get_assignment_files($ass->assignment_id);
            foreach($files as $file)
            {
                $this->assignment_model->remove_file($assignment_file_id);
            }

            $this->assignment_model->delete_assignment($ass->assignment_id);

            redirect('uppgifter/matris/' . $course->course_id . '/OK');
        }
        else
        {
            redirect('uppgift/ta_bort/' . $assignment_id);
        }
    }
    /**
     * Shows an error if the course does not exist.
     * @param int $course_id
     * @return course_record
     */
    function _check_course($course_id)
    {
        $course_id = intval($course_id);
        $course    = $this->course_model->get_course($course_id);
        if ($course === FALSE)
        {
            show_error('Kursen kunde ej hittas.' . intval($_POST['course_id']));
        }
        return $course;
    }

    /**
     * Callback function that checks if a YYYY-MM-DD string is a valid date.
     * @author Anders Johansson <anders@truenull.se>
     *
     * @param string $date
     * @return bool
     */
    function _check_date($date)
    {
        // Expecting YYYY-MM-DD
        $ymd = explode('-', $date, 3);
        if (count($ymd) === 3)
        {
            return checkdate(intval($ymd[1]), intval($ymd[2]), intval($ymd[0]));
        }
        return FALSE;
    }

    /**
     * Converts time to unix time.
     * @param type $date
     * @param type $hour
     * @param type $minute
     * @param type $second
     * @return int
     */
    function _get_time($date, $hour = 22, $minute = 59, $second = 59)
    {
        $ymd   = explode('-', $date, 3);
        $year  = intval($ymd[0]);
        $month = intval($ymd[1]);
        $day   = intval($ymd[2]);
        
        return mktime($hour, $minute, $second, $month, $day, $year);
    }

    function _set_uppgift_formvalidation()
    {
        $this->load->library('form_validation');

        // Validation rules
        $this->form_validation->set_rules('name', 'namn',
                                          'trim|required|max_length[50]|prep_for_form');
        $this->form_validation->set_rules('desc', 'beskrivning',
                                          'trim|max_length[8000]|prep_for_form');
        $this->form_validation->set_rules('number', 'uppgiftsnummer',
                                          'required|integer|is_natural|less_than[255]');
        $this->form_validation->set_rules('max_points', 'poängantal',
                                          'required|integer|is_natural|less_than[255]|');
        $this->form_validation->set_rules('files', 'fillistan', 'prep_for_form');
        $this->form_validation->set_rules('course_id', 'kurs id',
                                          'required|integer');
        $this->form_validation->set_rules('due_date', 'sista inlämningsdag',
                                          'required|callback__checkdate|prep_for_form');

        // Custom error message for date
        $this->form_validation->set_message('due_date',
                                            'Du måste fylla i ett '
                . 'giltigt datum på formen YYYY-MM-DD'
        );
        $this->form_validation->set_error_delimiters('<p class="form-control">',
                                                     '');
    }

    function _update_assignment($data, $assignment_id)
    {
        $this->assignment_model->set_desc($data['desc'],
                                          $assignment_id);
        $this->assignment_model->set_due_date($due_date,
                                              $assignment_id);
        $this->assignment_model->set_max_points($data['max_points'],
                                                $assignment_id);
        $this->assignment_model->set_name($data['name'],
                                          $assignment_id);
        $this->assignment_model->set_number($data['number'],
                                            $assignment_id);
        $this->assignment_model->set_mandatory($data['mandatory'],
                                               $assignment_id);
        $this->_update_files($data['files'], $assignment_id);
    }

    function _update_files($files, $assignment_id)
    {
        // do files
        $files = explode("\n", $files);
        $this->assignment_model->set_assignment_files($files, $assignment_id);
    }

}

// End of file uppgift.php
// Location: ./controllers/uppgift.php