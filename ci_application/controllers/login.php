<?php
/**
 * This file contains the controller for the registration pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The registration pages of the application.
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Login extends CI_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        check_db_version();
        
        $this->load->library('session');
    }

    /**
     * Shows the login form.
     * 
     */
    public function index()
    {
        $this->load->view('header', array(
            'user_name' => '',
            'header'    => 'Login',
            'hideNav'   => TRUE,
            'right'     => 0,
            'left'      => 12
        ));
        $this->load->view('login_form');
        $this->load->view('footer');
    }

    /**
     * Processes the login form and shows status or redirects to assignments
     * if no errors.
     *
     */
    public function in()
    {
        $this->load->view('header', array(
            'user_name' => '',
            'header'    => 'Login',
            'hideNav'   => TRUE,
            'right'     => 0,
            'left'      => 12
        ));
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="help-block">',
                                                     '</p>');

        // Form validation rules
        $this->form_validation->set_rules(
                'jarvisemail', 'e-post', 'trim|required'
        );
        $this->form_validation->set_rules(
                'jarvispass', 'lösenord',
                'trim|required|min_length['
                . user_model::min_pass_length . ']|max_length['
                . user_model::max_pass_length
                . ']|callback__check_password[' . $_POST['jarvisemail'] . ']'
        );


        // Check form
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('login_form');
        }
        else
        {
            // Set session information
            $newdata = array(
                'user_id'   => $this->user_model->get_user_id($_POST['jarvisemail']),
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);

            $courses = $this->course_model->get_all_courses();
            if(empty($courses) && 
                    ($this->permission_model->is_admin($this->session->userdata['user_id'])
                    || $this->permission_model->is_teacher($this->session->userdata['user_id'])
                    || $this->permission_model->is_limited_teacher($this->session->userdata['user_id'])))
            {
                redirect('ny_kurs/', 'location');
            }
            redirect('uppgifter/', 'location');
        }

        $this->load->view('footer');
    }

    /**
     * Logs out the user.
     */
    public function out()
    {
        $this->session->sess_destroy();
        redirect('login/', 'location');
        $this->load->view('header', array('user_name' => ''));
        $this->load->view('logout_success');
        $this->load->view('footer');
    }

    /**
     * A callback that checks if the password is right. (functions that start with _ can't be browsed to)
     * @param string $email
     * @param string $password
     * @return boolean
     */
    public function _check_password($email, $password)
    {
        // Check username / password
        if ($this->user_model->validate_login($_POST['jarvisemail'],
                                              $_POST['jarvispass']) === FALSE)
        {
            $this->form_validation->set_message('_check_password',
                                                'Användarnamn eller lösenord var felaktigt.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

}

// End of file login.php
// Location: ./controllers/login.php
