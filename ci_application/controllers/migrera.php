<?php

/**
 * This file contains the controller for the registration pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The registration pages of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Migrera extends CI_Controller
{

    /**
     * 
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Shows the registration form.
     * 
     * @return view Register form
     */
    public function index()
    {
        $this->databas();
    }

    public function databas()
    {
        if ($this->db->table_exists('migrations'))
        {

        }

        $this->load->library('migration');
        $data              = array();
        $data['was_error'] = $this->db->current();
        $data['error'] = $this->migration->error_string();

        $this->load->view('header',
                          array(
            'header'  => 'Databasmigrering',
            'hideNav' => TRUE
        ));
        
        $this->load->view('migrations', $data);
        $this->load->view('footer');
    }
}

// End of file migrera.php
// Location: ./controllers/migrera.php