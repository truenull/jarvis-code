<?php
/**
 * This file contains the controller for the assignment pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The assignment pages of the application.
 *
 * @property turnin_model $turnin_model
 * @property upload_model $upload_model
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Fil extends My_Controller
{

    const HL_ENDING = '.highlighted';
    const default_error = '<h4>Tyvärr!</h4><p>Du har antingen inte tillåtelse att visa denna filen eller så finns den inte.';

    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->model('turnin_model');
        $this->load->model('upload_model');
    }

    public function ladda($user_id, $assignment_file_id)
    {
        $error = Fil::default_error;
        if ($this->_can_read_files($user_id, $assignment_file_id))
        {
            $assignment_file = $this->assignment_model->get_assignment_file($assignment_file_id);
            $data = array(
                'contents' => $this->_student_file_contents(
                        $error, $user_id, $assignment_file_id),
            'assignment_file_name' => ($assignment_file) ? $assignment_file->file_name : '');
            $this->load->view('uppgifter/code', $data);
        }
        else
        {
            echo ($error);
        }
    }

    public function nerladdning($user_id, $assignment_file_id)
    {
        $ass_file = $this->assignment_model->get_assignment_file($assignment_file_id);
        if($ass_file === FALSE)
            show_error ('Uppgiftsfilen kunde inte hittas.');
        $assignment = $this->assignment_model->get_assignment($ass_file->assignment_id);
        if($assignment === FALSE)
        {
            // should never happen due to ref. integrity
            show_error ('Uppgiftsfilens uppgift kunde inte hittas.'); 
        }

        if(!($this->user->user_id == $user_id
                || $this->permission_model->is_teacher($this->user->user_id, $assignment->course_id)
                || $this->permission_model->is_limited_teacher($this->user->user_id, $assignment->course_id)))
        {
            show_error ('Du har inte tillgång till denna fil.');
        }

        $path = $this->upload_model->get_student_file_path($user_id, $ass_file->assignment_file_id);

        // make up a name
        $name = $user_id.'-'.$ass_file->assignment_file_id.'_'.$ass_file->file_name;
        if(!file_exists($path))
        {
            show_error('Filen finns ej på disk.');
        }
        
        // Read the file's contents
        $data = file_get_contents($path);

        $this->load->helper('download');
        force_download($name, $data);
    }
    
    public function visa($user_id, $assignment_file_id)
    {
        $error = Fil::default_error;
        if ($this->_can_read_files($user_id, $assignment_file_id))
        {
            $ass_file   = $this->assignment_model->get_assignment_file($assignment_file_id);
            $assignment = $this->assignment_model->get_assignment($ass_file->assignment_id);
            $course     = $this->course_model->get_course($assignment->course_id);

            $data = array(
                'contents' => $this->_student_file_contents(
                        $error, $user_id, $assignment_file_id));

            // needed for headers
            $this->load->helper('highlight_helper');
            $this->load->view('header',
                              array(
                'header'        => 'Elevfil',
                'hideNav'       => FALSE,
                'extra_headers'        => array(luminous_head(), "<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>"),
                'course' => $course,
                'assignment_file_name' => htmlspecialchars($ass_file->file_name),
                'navCourses'    => $this->user_model->get_latest_courses($this->user->user_id),
                'right'         => 0,
                'left'          => 12
            ));

            $this->load->view('uppgifter/code', $data);
            $this->load->view('footer');
        }
        else
        {
            echo ($error);
        }
    }

    public function ta_bort($user_id, $assignment_file_id)
    {
        if ($user_id == $this->user->user_id)
        {
            $ass_file = $this->assignment_model->get_assignment_file($assignment_file_id);
            $ass = $this->assignment_model->get_assignment($ass_file->assignment_id);
            if($ass_file == FALSE)
            {
                show_error('Kunde ej hitta denna uppgiftsfil i databasen.');
            }
            $status = $this->turnin_model->get_assignment_status(
                    $user_id,
                    $ass_file->assignment_id);
            if($status->status == assignment_status::under_grading
                    ||$status->status == assignment_status::not_turned_in)
            {
                $this->turnin_model->delete_student_file(
                        $user_id,
                        $ass_file->assignment_file_id);

                $this->turnin_model->eval_assignment_status(
                        $this->user->user_id,
                        $ass_file->assignment_id);
                
                redirect('uppgifter/elev/'. $this->user->user_id.'/'. $ass->course_id);
            }
            else
            {
                show_error('Kan endast ta bort filer när status är Ej Inlämnad eller Under Granskning.');
            }
        }
        else
            show_error ('Endast ägaren kan ta bort en fil.');

    }
    
    /**
     * Checks if it's the current users file or if the user is a teacher in the course
     * and that there exists an assignment file with the id in question.
     * @param int $user_id
     * @param int $assignment_file_id
     * @return boolean
     */
    private function _can_read_files($user_id, $assignment_file_id)
    {
        $file = $this->assignment_model->get_assignment_file($assignment_file_id);
        if ($file === FALSE)
        {
            return FALSE;
        }
        $assignment = $this->assignment_model->get_assignment($file->assignment_id);
        if ($assignment === FALSE)
        {
            return FALSE;
        }

        if ($this->user->user_id == $user_id ||
        $this->permission_model->is_teacher($this->user->user_id, $assignment->course_id) ||
        $this->permission_model->is_limited_teacher($this->user->user_id,
                                                            $assignment->course_id)
        ) {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     *  Gets a string with the file contents of the assignment file or an error
     *  message.
     * @param string $error
     * @param int $user_id
     * @param int $assignment_file_id
     * @param bool $suppress_hl If true, do not highlight the file.
     * @return string
     */
    function _student_file_contents($error, $user_id, $assignment_file_id,
                                    $suppress_hl = FALSE)
    {
        $file = $this->upload_model->get_student_file_path(
                $user_id,
                $assignment_file_id);
        $hl_ending = upload_model::HL_ENDING;
        $file_contents = '';

        if (file_exists($file . $hl_ending))
        {
            $file_contents = file_get_contents($file . $hl_ending);
        }
        else if (file_exists($file))
        {
            $no_contents = file_get_contents($file);

            $this->load->helper('highlight_helper');
            if (!$suppress_hl)
            {
                $file_contents = highlight($no_contents);
            }
            else
            {
                $file_contents = highlight($no_contents, 'plain');
            }
            file_put_contents($file . $hl_ending, $file_contents);

        }
        else
        {
            $file_contents = $error;
        }
        return $file_contents;
    }

}

// End of file fil.php
// Location: ./controllers/fil.php