<?php
class Konto extends My_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('user_model');
	}

	public function index() {
		$this->auto();
	}

	public function auto($course_id = 0) {
		$this->load->view('header', array(
            'header' => 'Konto',
            'course' => $this->_get_course_or_default($course_id),
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
            'active'     => 3
        ));

        $this->load->view('konto', array(
        	'user' => $this->user
        ));

        $this->load->view('footer');
	}

	public function update_user() {
		$email = $this->input->post('email');
		$newpass = $this->input->post('newpass');
		$newpassrepeat = $this->input->post('newpassrepeat');
		$oldpass = $this->input->post('oldpass');

		$errors = array();
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Något är fel med email.';
		}

		if ($newpass && !($newpass == $newpassrepeat)) {
			$errors['newpass'] = 'Nya lösenorden stämmer inte överens.';
		}

		if (!$this->user_model->validate_login($this->user->email, $oldpass)) {
			$errors['oldpass'] = 'Fel nuvarande lösenord.';
		}

		if (count($errors) == 0) {
			if ($email) {
				$this->user_model->set_email($email, $this->user->user_id);
			}
			if ($newpass) {
				$this->user_model->set_password($newpass, $this->user->user_id);
			}
		}

		print json_encode($errors);
	}
}