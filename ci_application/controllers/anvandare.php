<?php
/**
 * This file contains the controller for the assignment pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The assignment pages of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Anvandare extends My_Controller
{
    /**
     * 
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
    }

    /**
     * Form for adding or changing an assignment to a course.
     * 
     * @param type $course_id
     */
    function andra($course_id, $user_id)
    {
        $course = $this->course_model->get_course($course_id);
        if($course == FALSE)
        {
            show_error('Kunde ej hitta kursen.');
        }
        
        if (!(($this->permission_model->is_admin($this->user->user_id))
                || ($this->permission_model->is_teacher(
                        $this->user->user_id,
                        $course_id))
                || ($this->permission_model->is_limited_teacher(
                        $this->user->user_id,
                        $course_id)))
                )
        {
            show_error('Du har inte befogenhet att ändra '
                    . 'studenters information i denna kurs.', 403);
        }
        $user = $this->user_model->get_user($user_id);
        if ($user === false)
        {
            show_error('Användaren kunde ej hittas.');
        }

        // New assignment
        $data = array(
            'user'          => $user,
            'course'        => $course,
            'name'          => $user->user_name,
            'email'         => $user->email,
            'user_is_admin' => $this->permission_model->is_admin($this->user->user_id),
            'is_admin'      => $this->permission_model->is_admin($user_id),
            'is_self'       => $this->user->user_id == $user_id
        );
  
        $this->load->view('header',
            array(
            'course'     => $course,
            'header'     => 'Ändra användare',
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id)
                )
        );

        $this->load->view('deltagare/edit_user', $data);
        $this->load->view('footer');
    }

    function mottag()
    {
        $this->_set_user_formvalidation();

        // Fetch users / course
        $course        = $this->course_model->get_course($_POST['course_id']);
        $user = $this->user_model->get_user($_POST['user_id']);

        // Run validation to populate set_value
        $validation = $this->form_validation->run();
        // fill data array
        $data = array(
            'user'          => $user,
            'course'        => $course,
            'name'          => set_value('namef'),
            'user_id'       => $user->user_id,
            'email'         => set_value('emailf'),
            'is_admin'      => set_value('is_admin'),
            'user_is_admin' => $this->permission_model->is_admin($this->user->user_id),
            'is_self'       => $this->user->user_id == $_POST['user_id']
        );
        //die(print_r($data));
        if ($validation == FALSE)
        {
            // Redisplay form
            $this->load->view('header',
                array(
                    'course'     => $course,
                    'header'     => 'Ändra användare',
                    'navCourses' => $this->user_model->get_latest_courses(
                        $this->user->user_id)
                )
            );
            $this->load->view('deltagare/edit_user', $data);
            $this->load->view('footer');
        }
        else
        {            
            $this->_update_user(
                        $data['email'],
                        $data['user_id'],
                        $data['name'],
                        $data['is_admin']
                    );

            redirect('uppgifter/matris/' . $course->course_id . '/'. 'OK');
        }
    }

    function _set_user_formvalidation()
    {
        $this->load->library('form_validation');

        // Validation rules
        $this->form_validation->set_rules('namef', 'namn',
                                          'trim|required|max_length[50]|prep_for_form');
        $this->form_validation->set_rules('emailf', 'email',
                                          'trim|valid_email|max_length[70]|prep_for_form');

        $this->form_validation->set_error_delimiters('<p class="form-control">',
                                                     '');
    }

    function _update_user($email, $user_id, $user_name, $admin)
    {
        $this->user_model->set_email($email, $user_id);

        $this->user_model->set_name($user_name, $user_id);

        // Only admins should be able to change admins and not himself.
        if($this->permission_model->is_admin($this->user->user_id)
                && $user_id != $this->user->user_id)
        {
            if($admin == FALSE)
            {
                $this->user_model->set_not_admin($user_id);
            }
            else
            {
                $this->user_model->set_admin($user_id);
            }
        }
    }
}

// End of file uppgift.php
// Location: ./controllers/uppgift.php