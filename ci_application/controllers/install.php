<?php

/**
 * This file contains the controller for the registration pages of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The registration pages of the application.
 *
 * @category Controllers
 * @package  Public
 * @author   Anders Johansson <anders@truenull.se>
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Install extends CI_Controller
{

    /**
     * 
     */
    function __construct()
    {
        parent::__construct();


        $this->config->load('config');
        $enable_install = $this->config->item('enable_install');
        if(!$enable_install)
        {
            show_error('Installationsskriptet är inte påslaget i '
                    . '.ci_application/config/config.php');
        }
    }

    /**
     * Shows the registration form.
     * 
     * @return view Register form
     */
    public function index()
    {
        redirect('install/valkommen');
    }

    public function valkommen()
    {
        $this->load->view('header',
                          array(
            'header'  => 'Välkommen',
            'hideNav' => TRUE
        ));
        $this->load->view('install/welcome');
        $this->load->view('footer');
    }

    public function databas()
    {
        if ($this->db->table_exists('migrations'))
        {
            // Reset the version count for this rollup release.
            $this->db->update('migrations', array('version' => 0));
        }
        $this->load->library('migration');
        $data              = array();
        $data['was_error'] = $this->migration->current();

        $data['error'] = $this->migration->error_string();


        $this->load->view('header',
                          array(
            'header'  => 'Databasinitiering',
            'hideNav' => TRUE
        ));
        $this->load->view('install/database', $data);
        $this->load->view('footer');
    }

    public function exempel()
    {
        if (ENVIRONMENT != 'development')
        {
            show_error('Exempeldata är enbart tillgängligt i development läget.');
        }
        $sql       = file_get_contents(APPPATH . 'sql/sample_data.sql');
        $sql_lines = explode(';', $sql);
        unset($sql);

        $this->db->trans_start();
        foreach ($sql_lines as $query)
        {
            if (trim($query) !== '')
            {
                $this->db->query($query . ';');
            }
        }
        $this->db->trans_complete();

        $this->load->view('header',
                          array(
            'header'  => 'Välkommen',
            'hideNav' => TRUE
        ));
        $this->load->view('install/devel_info');
        $this->load->view('footer');
    }

    public function admin()
    {
        // Only the system user should exist in the db at this point.
        if($this->db->count_all('users') != 1)
        {
            show_error('Mer än en användare finns i databasen. Denna sida ska ej kunna åtkommas då.');
        }
        
        $this->load->view('header', array(
            'header'  => 'Skapa administratör',
            'hideNav' => TRUE
        ));
        $this->load->view('install/user_form');
        $this->load->view('footer');
    }

    public function administrator()
    {
        // Only the system user should exist in the db at this point.
        if($this->db->count_all('users') != 1)
        {
            show_error('Mer än en användare finns i databasen. Denna sida ska ej kunna åtkommas då.');
        }
        $this->_set_admin_rules();
        $this->load->view('header',
                          array(
            'header'  => 'Skapa administratör',
            'hideNav' => TRUE
        ));
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('install/user_form');
        }
        else
        {
            $email = set_value('email');
            $name = set_value('name');
            $pass = set_value('jarvispass');
            
            $user_id = $this->user_model->create_user($name, $email, $pass);
            $this->user_model->set_admin($user_id);

            $this->_send_email($email, $name);

            $this->load->view('install/user_created');
        }
        $this->load->view('footer');
    }

    public function filer()
    {
        $this->load->view('header',
                          array(
            'header'  => 'Filrättigheter',
            'hideNav' => TRUE
        ));

        $this->config->load('upload');
        $upload_path = realpath($this->config->item('upload_path'));
        $writable = is_writable($upload_path);

        $this->load->view('install/file_io', array(
            'upload_path' => $upload_path,
            'writable' => $writable));

        $this->load->view('footer');
    }
    
    public function _send_email($email, $name)
    {
        $url     = site_url();
        $subject = 'Installation av Jarvis';
        $message = <<<EOT
<h1>Hejsan $name!</h1>
<p>Detta email är en del av installationsprocessen på 
{unwrap}<a href="$url">$url</a>{/unwrap}.
<p>Eftersom du fått detta mejl så vet du att mailfunktionen är rätt konfigurerad.
<p>Mvh,<br/>
e-lärsystemet Jarvis
EOT;

        $this->config->load('email');
        $this->load->library('email');

        $this->email->from($this->config->item('system_from'));
        $this->email->to($email);

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
        return $this->email->print_debugger();
    }

    public function _set_admin_rules()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Username', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email',
                'trim|required|valid_email|max_length[70]|is_unique[users.email]');
        $this->form_validation->set_rules('jarvispass', 'Password',
                'trim|required|min_length[' . user_model::min_pass_length
                . ']|max_length[' . user_model::max_pass_length . ']');
    }
}

// End of file install.php
// Location: ./controllers/install.php