<?php
/**
 * This file contains the controller for the course creation page of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Daniel Lundqvist <luda09yl@student.hj.se>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class Ny_kurs extends My_Controller
{

    /**
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     *  Shows the new course form
     *
     * @return view New course
     */
    public function index()
    {
        $this->auto();
    }

    /**
     * Since we can't have parameters on index function, we use auto (select)
     * @param int $course_id
     */
    public function auto($course_id = 0)
    {
        if (!$this->permission_model->is_admin($this->user->user_id))
            show_error('Du har inte tillåtelse att skapa ny kurs.');

        $course = $this->_get_course_or_default($course_id, $this->user->user_id);

        $this->load->view('header',
                          array(
            'user_name'  => '',
            'header'     => 'Ny kurs',
            'hideNav'    => FALSE,
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($this->user->user_id),
        ));
        $this->load->view('new_course', array('first_visit'  => TRUE));
        $this->load->view('footer');
    }

    /**
     * Processes the create new course form and shows status or redirects to edit course
     * if no errors.
     */
    public function validate($course_id = 0)
    {
        if (!$this->permission_model->is_admin($this->user->user_id))
            show_error('Du har inte tillåtelse att skapa ny kurs.');

        $course = $this->_get_course_or_default($course_id, $this->user->user_id);

        $this->load->view('header',
                          array(
            'user_name' => '',
            'header'    => 'Ny kurs',
            'hideNav'    => FALSE,
            'course'     => $course,
            'navCourses' => $this->user_model->get_latest_courses($course->course_id),
            'right'     => 0,
            'left'      => 12
        ));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('coursetag', 'kurstag',
                                          'trim|required|max_length[10]');
        $this->form_validation->set_rules('coursename', 'kursnamn',
                                          'trim|required|max_length[40]');
        $this->form_validation->set_rules('courseend', 'slutdatum',
                                          'trim|required|callback__check_date');
        $this->form_validation->set_rules('coursedescription',
                                          'kursbeskrivning', 'trim');
        
        $this->form_validation->set_error_delimiters('<p class="form-control">',
                                                     '');

        if ($this->form_validation->run() == FALSE)
            $this->load->view('new_course');
        else
        {
            // Convert our date to the format we intend to store it in.
            $endtime = strtotime($_POST['courseend']);
            // Create the course in the database. This function returns the course id.
            $course_id = $this->course_model->create_course($_POST['coursetag'],
                                                            $_POST['coursename'],
                                               $_POST['coursedescription'],
                                               $endtime);
            redirect('uppgift/ny/' . $course_id, 'location');
        }

        $this->load->view('footer');
    }

    /**
     * A callback that checks if the the date is in the correct format.
     * Functions that start with _ can't be browsed to.
     * 
     * @return boolean
     *
     * TODO: Additional checks need to be made.
     * Example: 'd' passes this test while 'dd' does not.
     */
    public function _check_date()
    {
        $arr = explode('-', $_POST['courseend']);

        // Remove all empty elements and make sure that we only parse numbers.
        foreach ($arr as $key => $value)
        {
            if (empty($value))
            {
                unset($arr[$key]);
                continue;
            }
            if (!is_numeric($value))
            {
                $this->form_validation->set_message('_check_date',
                                                    'Slutdatum är ej i korrekt format. Använd endast siffror i formatet YYYY-MM-DD.');
                return FALSE;
            }
        }

        // Make sure we have 3 elements.
        if (count($arr) != 3)
        {
            $this->form_validation->set_message('_check_date',
                                                'Slutdatum är ej i korrekt format. Använd formatet YYYY-MM-DD.');
            return FALSE;
        }

        // Check if we have a valid date.
        $result = checkdate($arr[1], $arr[2], $arr[0]); // $month, $day, $year
        if ($result == TRUE)
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('_check_date',
                                                'Slutdatum har ett ogiltigt datum.');
            return FALSE;
        }
    }

}

// End of file ny_kurs.php
// Location: ./controllers/ny_kurs.php
