<?php

$dbuser = "root";
$dbpassword = "toor";

$dsn = 'mysql:dbname=jarvis;host=127.0.0.1';

$numStudents = 100;
$numAssignments = 10;

$studentIdFile = "/home/u/students.userid";

function printSQL($sql)
{
    if (TRUE)
    {
        echo $sql . "\n";
    }
}

function addCourse($db, $name)
{
    // Add a new course
    $q = "INSERT INTO `jarvis`.`courses` (`course_id`, `course_tag`, `course_name`, `course_description`) VALUES (NULL, 'TEST-$name', 'Test course #$name', NULL);";
    printSQL($q);
    $db->exec($q);
    return $db->lastInsertId();
}

function addStudents($db, $storageFile, $amount, $cid)
{
// Add students
    if (file_exists($storageFile))
    {
//load from file
        return unserialize(file_get_contents($storageFile));
    }

    $ids = array();
// add new
    for ($i = 0; $i < $amount; $i++)
    {
        $q = "INSERT INTO `jarvis`.`users`"
            . "(`user_id`, `user_name`, `email`, `password_hash`, `last_login`) "
                . "VALUES (NULL, 'Iron man "
                . $i . "', 'im" . $i . "@jarvis.invalid', 'invalid', '0');";
        printSQL($q);
        $db->exec($q);
        $ids[] = $db->lastInsertId();
    }
    file_put_contents($storageFile, serialize($ids));
    return $ids;
}

try {
    $dbh = new PDO($dsn, $dbuser, $dbpassword, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));

    //$courseID = addCourse($dbh, $numAssignments);
    $courseID = 8;
    //$studentID = addStudents($dbh, $studentIdFile, $numStudents);
// Add assignments to course
    for($i = 0; $i < $numAssignments; $i++)
{
	$q = "INSERT INTO `jarvis`.`assignments` (`assignment_id`, "
                . "`assignment_number`, `assignment_name`, `assignment_desc`, "
                . "`course_id`) VALUES (NULL, '"
                . ($i + 1) . "', 'Element #"
                . $i . "', 'Blah blah blah....', '"
                . $courseID . "');";
        printSQL($q);
        $dbh->exec($q);
}


// Connect users to course
    /*
     * // just commented out since we're adding assignments separately
      for($i = 0; $i < count($studentID); $i++)
      {
        $q= "INSERT INTO `jarvis`.`registrations` (`user_id`, `course_id`, `access_level`) VALUES ('"
        . $studentID[$i] . "', '$courseID', '10');";
        printSQL($q);
        $dbh->exec($q);
      }
     */
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
        exit();
}

