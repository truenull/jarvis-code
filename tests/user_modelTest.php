<?php
/**
 * 
 */
class User_modelTest extends PHPUnit_Framework_TestCase
{

    /**
     *
     * @var string $name
     */
    public $name     = 'Bob Bobbson';

    /**
     *
     * @var string $email
     */
    public $email    = 'bob@tester.invalid';

    /**
     *
     * @var string $password
     */
    public $password = 'testing';

    /**
     *
     * @var My_Controller $CI
     */
    private $CI;

    public function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('database');
    }

    public function tearDown()
    {

    }

    public function testCreateUser()
    {
      $name     = $this->name;
      $email    = $this->email;
      $password = $this->password;

      $this->CI->load->model('user_model');
      try
      {
      $user_id = $this->CI->user_model->create_user($name, $email, $password);
      }
      catch (Exception $e)
      {
      $this->assertTrue(false, 'Something failed with database insertion.');
      }

      $this->assertGreaterThan(0, $user_id, 'User not created successfully');
      $actual_user = $this->CI->user_model->get_user($user_id);


      // Compare inserted values
      $actual_name = $actual_user->user_name;
      $actual_email = $actual_user->email;
      $actual_hash  = $actual_user->password_hash;

      $this->assertEquals($name, $actual_name, 'Username did not match.');
      $this->assertEquals($email, $actual_email, 'Email did not match.');
      $hash         = $this->CI->user_model->hash_password($password, $email);
      $this->assertEquals($hash, $actual_hash,
      'Hashed password did not match.'
      );

      // Test wrong password hash comparison
      $this->assertNotEquals(
      $this->CI->user_model->hash_password('Go Saints!', $email),
      $actual_user->password_hash,
      'Hashed password matched with wrong password.');

      $this->assertNotEquals(
      $this->CI->user_model->hash_password($password, 'steve@apple.com'),
      $actual_user->password_hash,
      'Hashed password matched with wrong email.');

      $this->assertNotEquals(
      $this->CI->user_model->hash_password('Go Saints!', 'steve@apple.com'),
      $actual_user->password_hash,
      'Hashed password matched with wrong email.');

      // Test get_user_id()
      $gotten_uid = $this->CI->user_model->get_user_id($email);
      $this->assertEquals($user_id, $gotten_uid,
      'user_model->get_user_id'
      . 'returned wrong user id'
      );

      // Test validate password.
      $valid_password = $this->CI->user_model->validate_login($email,
      $password);
      $this->assertTrue($valid_password,
      'Password did not validate when calling validate_login');


      $invalid_password = $this->CI->user_model->validate_login($email,
      'Go Saints!');
      $this->assertFalse($invalid_password,
      'Password validated when calling validate_login');

        $this->CI->user_model->remove_user($user_id);
        $this->assertFalse($this->CI->user_model->get_user($user_id),
      'User could be fetched after removal.');
    }

    public function testCSVCreateUsers()
    {
        $first_name = 'Bob';
        $last_name  = 'Bobbson';
        $email      = 'bob@tester.invalid';

        $afirst_name = 'Annie';
        $alast_name  = 'Annison';
        $aemail      = 'Annie@tester.invalid';

        $num_users = 2;

        $csv = <<<EOD
$first_name;$last_name;$email;
$afirst_name;$alast_name;$aemail;

EOD;
        $ids         = $this->CI->user_model->create_from_csv($csv);

        $this->assertEquals($num_users, count($ids),
                                              'Different count of ids than in csv');

        $ids2 = $this->CI->user_model->create_from_csv($csv);

        $this->assertEquals($num_users, count($ids2),
                                              'Different count of ids than in csv');

        foreach ($ids as $id)
        {
            $this->CI->user_model->remove_user($id);
            $this->assertFalse($this->CI->user_model->get_user($id),
                                                               'Could still get user after removing.');
        }
    }

    public function testResetTokens()
    {
                $first_name = 'Bob';
        $last_name  = 'Bobbson';
        $email      = 'bob@tester.invalid';

        $afirst_name = 'Annie';
        $alast_name  = 'Annison';
        $aemail      = 'Annie@tester.invalid';

        $num_users = 2;

        $csv = <<<EOD
$first_name;$last_name;$email;
$afirst_name;$alast_name;$aemail;

EOD;
        $ids = $this->CI->user_model->create_from_csv($csv);

        $this->assertEquals($num_users, count($ids),
                                              'Different count of ids than in csv');

        $user = $this->CI->user_model->get_user($ids[0]);
        $this->assertTrue($user !== FALSE);

        $failed_token = $this->CI->user_model->get_reset_token($user->user_id);
        $this->assertFalse($failed_token,
                           'We have not created any reset tokens for the user we just created yet.');

        $created_token = $this->CI->user_model->create_reset_token_by_mail($user->email);
        $this->assertTrue($created_token, 'Failed creating reset token');

        $reset_token = $this->CI->user_model->get_reset_token($ids[0]);
        $this->assertTrue($reset_token !== FALSE,
                          'Failed getting created token.');

        $token_exists = $this->CI->user_model->reset_token_exists(
                $ids[0], $reset_token->reset_token);
        $this->assertTrue($token_exists,
                        'Reset token should exist if we were able to get it.');

        $token_by_token = $this->CI->user_model->get_reset_token_by_token($reset_token->reset_token);
        $this->assertEquals($reset_token, $token_by_token,
                            'Token by token is not the same as token by id.');

        foreach ($ids as $id)
        {
            $this->CI->user_model->remove_user($id);
            $this->assertFalse($this->CI->user_model->get_user($id),
                                                               'Could still get user after removing.');
        }
    }

}
