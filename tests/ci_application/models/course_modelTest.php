<?php

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2013-09-21 at 21:41:14.
 */
class course_modelTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var course_model
     */
    protected $m;

    /**
     *
     * @var My_Controller $CI
     */
    protected $CI;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('database');
        $m        = &$this->CI->course_model;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {

    }

    /**
     * @covers course_model::create_course
     * @todo   Implement testCreate_course().
     */
    public function testCreate_course()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::delete_course
     * @todo   Implement testDelete_course().
     */
    public function testDelete_course()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::get_all_courses
     * @todo   Implement testGet_all_courses().
     */
    public function testGet_all_courses()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::get_course
     * @todo   Implement testGet_course().
     */
    public function testGet_course()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::set_desc
     * @todo   Implement testSet_desc().
     */
    public function testSet_desc()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::set_end_time
     * @todo   Implement testSet_end_time().
     */
    public function testSet_end_time()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::set_name
     * @todo   Implement testSet_name().
     */
    public function testSet_name()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::set_tag
     * @todo   Implement testSet_tag().
     */
    public function testSet_tag()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::register_teacher
     * @todo   Implement testRegister_teacher().
     */
    public function testRegister_teacher()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::register_student
     * @todo   Implement testRegister_student().
     */
    public function testRegister_student()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::register_limited
     * @todo   Implement testRegister_limited().
     */
    public function testRegister_limited()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers course_model::unregister
     * @todo   Implement testUnregister().
     */
    public function testUnregister()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}
