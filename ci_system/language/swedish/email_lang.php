<?php

$lang['email_must_be_array'] = "E-postvalideringen kräver en array.";
$lang['email_invalid_address'] = "Ogiltig e-postadress: %s";
$lang['email_attachment_missing'] = "Kan inte hitta följande e-postbilaga: %s";
$lang['email_attachment_unreadable'] = "Det gick inte att öppna bilagan: %s";
$lang['email_no_recipients'] = "Du måste inkludera mottagare: Till, Kopia, eller Hemlig kopia";
$lang['email_send_failure_phpmail'] = "Det går inte att skicka mail via PHP mail(). Servern kanske inte är konfigurerad att skicka mail med denna metod.";
$lang['email_send_failure_sendmail'] = "Det går inte att skicka mail via PHP Sendmail. Servern kanske inte är konfigurerad att skicka mail med denna metod.";
$lang['email_send_failure_smtp'] = "Det går inte att skicka mail via PHP SMTP. Servern kanske inte är konfigurerad att skicka mail med denna metod.";
$lang['email_sent'] = "Ditt meddelande har sänts via följande protokoll: %s";
$lang['email_no_socket'] = "Det går inte att öppna en socket till Sendmail. Kontrollera inställningarna.";
$lang['email_no_hostname'] = "Du har inte angivit något SMTP-värdnamn.";
$lang['email_smtp_error'] = "Följande SMTP fel uppstod: %s";
$lang['email_no_smtp_unpw'] = "Fel: Du måste tilldela ett SMTP-användarnamn och lösenord.";
$lang['email_failed_smtp_login'] = "Det gick inte att skicka AUTH LOGIN kommando. Fel: %s";
$lang['email_smtp_auth_un'] = "Det gick inte att verifiera användarnamn. Fel: %s";
$lang['email_smtp_auth_pw'] = "Det gick inte att verifiera lösenordet. Fel: %s";
$lang['email_smtp_data_failure'] = "Det gick inte att skicka data: %s";
$lang['email_exit_status'] = "Exit-statuskod: %s";


/* End of file email_lang.php */
/* Location: ./system/language/english/email_lang.php */