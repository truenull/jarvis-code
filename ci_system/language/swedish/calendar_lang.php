<?php

$lang['cal_su']			= "Sö";
$lang['cal_mo']			= "Må";
$lang['cal_tu']			= "Ti";
$lang['cal_we']			= "On";
$lang['cal_th']			= "To";
$lang['cal_fr']			= "Fr";
$lang['cal_sa']			= "Lö";
$lang['cal_sun']		= "Sön";
$lang['cal_mon']		= "Mån";
$lang['cal_tue']		= "Tis";
$lang['cal_wed']		= "Ons";
$lang['cal_thu']		= "Tor";
$lang['cal_fri']		= "Fre";
$lang['cal_sat']		= "Lör";
$lang['cal_sunday']		= "Söndag";
$lang['cal_monday']		= "Måndag";
$lang['cal_tuesday']	= "Tisdag";
$lang['cal_wednesday']	= "Onsdag";
$lang['cal_thursday']	= "Torsdag";
$lang['cal_friday']		= "Fredag";
$lang['cal_saturday']	= "Lördag";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "Maj";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Okt";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "Januari";
$lang['cal_february']	= "Februari";
$lang['cal_march']		= "Mars";
$lang['cal_april']		= "April";
$lang['cal_mayl']		= "Maj";
$lang['cal_june']		= "Juni";
$lang['cal_july']		= "Juli";
$lang['cal_august']		= "Augusti";
$lang['cal_september']	= "September";
$lang['cal_october']	= "Oktober";
$lang['cal_november']	= "November";
$lang['cal_december']	= "December";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */