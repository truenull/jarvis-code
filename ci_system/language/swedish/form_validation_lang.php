<?php

$lang['required']			= "Du måste fylla i %s.";
$lang['isset']				= "Fältet %s måste ha ett värde.";
$lang['valid_email']	= "Fältet %s måste innehålla en giltig e-postadress.";
$lang['valid_emails']	= "Fältet %s måste innehålla enbart giltiga e-postadresser.";
$lang['valid_url']		= "Fältet %s måste innehålla en giltig URL.";
$lang['valid_ip']			= "Fältet %s måste innehålla en giltig IP-adress.";
$lang['min_length']		= "Innehållet i fältet %s måste vara minst %s tecken långt.";
$lang['max_length']		= "Innehållet i fältet %s får inte vara längre än %s tecken långt.";
$lang['exact_length']	= "Innehållet i fältet %s måste vara exakt %s tecken långt.";
$lang['alpha']				= "Fältet %s får enbart innehålla bokstäver.";
$lang['alpha_numeric']= "Fältet %s får enbart innehålla bokstäver och siffror.";
$lang['alpha_dash']		= "Fältet %s får enbart innehålla bokstäver, siffror, understreck och bindestreck.";
$lang['numeric']			= "Fältet %s får enbart innehålla siffror.";
$lang['is_numeric']		= "Fältet %s får enbart innehåll numeriska värden.";
$lang['integer']			= "Fältet %s får enbart innehålla heltal.";
$lang['regex_match']	= "Innehållet i fältet %s är i fel format.";
$lang['matches']			= "Fältet %s matchar inte fältet %s.";
$lang['is_unique'] 		= "Fältet %s måste innehålla ett unikt värde.";
$lang['is_natural']		= "Fältet %s får enbart innehålla positiva tal.";
$lang['is_natural_no_zero']	= "Fältet %s måste innehålla ett tal större än noll.";
$lang['decimal']			= "Fältet %s måste innheålla ett decimaltal.";
$lang['less_than']		= "Fältet %s måste innehålla ett tal mindre än %s.";
$lang['greater_than']	= "Fältet %s måste innehålla ett tal större än %s.";


/* End of file form_validation_lang.php */
/* Location: ./application/language/swedish/form_validation_lang.php */