<?php

$lang['db_invalid_connection_str'] = 'Det gick inte att avgöra vilka databasinställningar som ska användas utifrån den connection string du har angivit.';
$lang['db_unable_to_connect'] = 'Det går inte att ansluta till databasen med de angivna inställningarna.';
$lang['db_unable_to_select'] = 'Det går inte att välja den angivna databasen: %s';
$lang['db_unable_to_create'] = 'Det går inte att skapa den angivna databasen: %s';
$lang['db_invalid_query'] = 'Databasförfrågan du angav inte är giltig.';
$lang['db_must_set_table'] = 'Du måste ställa in databas-tabellen för att användas med din förfrågan.';
$lang['db_must_use_set'] = 'Du måste använda "set" för att uppdatera en post.';
$lang['db_must_use_index'] = 'Du måste specificera ett index att matcha mot för batch-uppdatering.';
$lang['db_batch_missing_index'] = 'En eller flera av de rader som lämnats för batch-uppdatering saknar det specificerade indexet.';
$lang['db_must_use_where'] = 'Uppdateringar är inte tillåtna om de inte innehåller en "where"-sats.';
$lang['db_del_must_use_where'] = 'Deletes är inte tillåtna om de inte innehåller ett "where" eller "like".';
$lang['db_field_param_missing'] = 'För att hämta fält krävs namnet på tabellen som en parameter.';
$lang['db_unsupported_function'] = 'Den här funktionen är inte tillgänglig för databasen du använder.';
$lang['db_transaction_failure'] = 'Transaktion misslyckades: Rollback har utförts.';
$lang['db_unable_to_drop'] = 'Det gick inte att ta bort den angivna databasen.';
$lang['db_unsuported_feature'] = 'Funktionen fungerar inte med den databas du använder';
$lang['db_unsuported_compression'] = 'Filenkomprimeringsformatet du valde stöds inte av servern.';
$lang['db_filepath_error'] = 'Det går inte att skriva data till sökvägen du angett.';
$lang['db_invalid_cache_path'] = 'Cache-sökvägen du angav är ogiltig eller skrivskyddad.';
$lang['db_table_name_required'] = 'Ett tabellnamn krävs för operationen.';
$lang['db_column_name_required'] = 'Ett kolumnnamn krävs för operationen.';
$lang['db_column_definition_required'] = 'En kolumndefinition krävs för operationen.';
$lang['db_unable_to_set_charset'] = 'Det gick inte att ställa in teckenuppsättning %s för klientanslutning.';
$lang['db_error_heading'] = 'Ett databasfel uppstod.';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */