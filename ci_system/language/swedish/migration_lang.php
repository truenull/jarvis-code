<?php

$lang['migration_none_found']			= "Inga migreringar kunde hittas.";
$lang['migration_not_found']			= "Denna migrering kunde ej hittas.";
$lang['migration_multiple_version']		= "Det finns flera migreringar med denna version: %d.";
$lang['migration_class_doesnt_exist']	= "Migreringsklassen \"%s\" kunde ej hittas.";
$lang['migration_missing_up_method']	= "Migreringsklassen \"%s\" saknar en 'up' metod.";
$lang['migration_missing_down_method']	= "Migreringsklassen \"%s\" saknar en 'down' metod.";
$lang['migration_invalid_filename']		= "Migreringsen \"%s\" har ett ogiltigt filnamn.";


/* End of file migration_lang.php */
/* Location: ./system/language/swedish/migration_lang.php */
