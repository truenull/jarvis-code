<?php

$lang['profiler_database']		= 'DATABAS';
$lang['profiler_controller_info'] = 'KLASS/METOD';
$lang['profiler_benchmarks']	= 'PRESTANDATEST';
$lang['profiler_queries']		= 'FÖRFRÅGNINGAR';
$lang['profiler_get_data']		= 'GET-DATA';
$lang['profiler_post_data']		= 'POST-DATA';
$lang['profiler_uri_string']	= 'URI-STRING';
$lang['profiler_memory_usage']	= 'MINNESANVÄNDNING';
$lang['profiler_config']		= 'CONFIG-VARIABLER';
$lang['profiler_headers']		= 'HTTP-HEADERS';
$lang['profiler_no_db']			= 'Databasdrivrutinen är inte laddad';
$lang['profiler_no_queries']	= 'Inga förfrågningar gjordes';
$lang['profiler_no_post']		= 'Det finns ingen POST-data';
$lang['profiler_no_get']		= 'Det finns ingen GET-data';
$lang['profiler_no_uri']		= 'Det finns ingen URI-data';
$lang['profiler_no_memory']		= 'Minnesanvändning är inte tillgängligt';
$lang['profiler_no_profiles']	= 'Ingen Profile-data - alla Profiler-sektioner har inaktiverats.';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */