<?php

$lang['ftp_no_connection']			= "Kan inte hitta något giltigt anslutnings-ID. Kontrollera att du är ansluten innan du utför några filrutiner.";
$lang['ftp_unable_to_connect']		= "Det går inte att ansluta till din FTP-server med angivet värdnamn.";
$lang['ftp_unable_to_login']		= "Kan inte logga in på din FTP-server. Kontrollera ditt användarnamn och lösenord.";
$lang['ftp_unable_to_makdir']		= "Det går inte att skapa katalogen du har angett.";
$lang['ftp_unable_to_changedir']	= "Kan inte byta katalog.";
$lang['ftp_unable_to_chmod']		= "Det går inte att ange filrättigheter. Kontrollera din sökväg. OBS: Denna funktion är endast tillgänglig i PHP 5 eller högre.";
$lang['ftp_unable_to_upload']		= "Det går inte att ladda upp den angivna filen. Kontrollera din sökväg.";
$lang['ftp_unable_to_download']	= "Det går inte att ladda ned den angivna filen. Kontrollera din sökväg.";
$lang['ftp_no_source_file']			= "Kan inte hitta källfilen. Kontrollera din sökväg.";
$lang['ftp_unable_to_rename']		= "Kan inte byta namn på filen.";
$lang['ftp_unable_to_delete']		= "Kan inte ta bort filen.";
$lang['ftp_unable_to_move']			= "Det gick inte att flytta filen. Se till målkatalogen finns.";


/* End of file ftp_lang.php */
/* Location: ./system/language/english/ftp_lang.php */